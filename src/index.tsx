// import * as serviceWorker from './serviceWorker';
import ReactDOM from 'react-dom';
import React, { Suspense } from 'react';
import { AuthProvider } from 'context/auth';
import { ActualApp } from 'app/ActualApp';
import { Loading } from 'components/Loading';
import { messages } from 'messages';
import { IntlProvider } from 'react-intl';
import { useLocalStorage } from 'hooks/useLocalStorage';
import { BrowserRouter as Router } from 'react-router-dom';

let browserLocale = navigator.languages && navigator.languages[0];
if (browserLocale !== 'ru-RU' && browserLocale !== 'en-US')
  browserLocale = 'en-US';

const App: any = () => {
  const [locale, setLocale] = useLocalStorage('locale');
  if (!locale) setLocale(browserLocale);
  else if (locale !== 'ru-RU' && locale !== 'en-US') setLocale('en-US', true);

  return (
    <IntlProvider
      locale={locale}
      key={locale}
      defaultLocale='en-US'
      messages={messages[locale]}
      timeZone='Europe/Moscow'
    >
      <Router>
        <Suspense fallback={<Loading />}>
          <AuthProvider>
            <ActualApp />
          </AuthProvider>
        </Suspense>
      </Router>
    </IntlProvider>
  );
};

ReactDOM.render(<App />, document.getElementById('root'));

// serviceWorker.unregister();
