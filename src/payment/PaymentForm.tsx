import React from 'react';
import { FormikProps, Form, Field } from 'formik';
import { useIntl } from 'react-intl';
import { PaymentValues } from '../types';
import { makeStyles, Theme, InputAdornment } from '@material-ui/core';
import { TextField } from 'formik-material-ui';
import { LoadingButton } from '../components/LoadingButton';

const useStyles = makeStyles((theme: Theme) => ({
  form: {
    width: '100%',
    marginTop: theme.spacing(2)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

export const PaymentForm = (props: FormikProps<PaymentValues>) => {
  const { isSubmitting } = props;
  const classes = useStyles();
  const intl = useIntl();

  return (
    <Form className={classes.form}>
      <Field
        label={intl.formatMessage({ id: 'sum' })}
        component={TextField}
        id='amount'
        name='amount'
        variant='outlined'
        margin='normal'
        required
        fullWidth
        InputProps={{
          endAdornment: <InputAdornment position='start'>₽</InputAdornment>
        }}
      />

      <LoadingButton size='large' title='replenish' isLoading={isSubmitting} />
    </Form>
  );
};
