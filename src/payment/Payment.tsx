import React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { PaymentProps } from '../types';
import {
  Card,
  ListItem,
  ListItemText,
  Button,
  Grid,
  Box,
  Link
} from '@material-ui/core';

interface LinkButtonProps {
  payStatus: string;
  payUrl: string;
}

interface AmountProps {
  amount: number;
  refunded: number;
  payStatus: string;
}

export const Payment: React.FC<PaymentProps> = ({
  amount,
  payStatus,
  payUrl,
  refunded,
  data
}) => {
  return (
    <Box mt={2}>
      <Card>
        <Grid container alignItems='center' justify='space-between'>
          <Amount
            {...{ amount, payStatus, refunded: data && data.Amount / 100 }}
          />
          <LinkButton {...{ payStatus, payUrl }} />
        </Grid>
      </Card>
    </Box>
  );
};

const Amount: React.FC<AmountProps> = ({ amount, refunded, payStatus }) => {
  const values = {
    amount: <FormattedNumber style={`currency`} currency='RUB' value={amount} />
  };

  return (
    <Grid item>
      <ListItem>
        <ListItemText
          secondary={<FormattedMessage id={payStatus} values={{ refunded }} />}
          primary={<FormattedMessage id='amount' values={values} />}
        />
      </ListItem>
    </Grid>
  );
};

const LinkButton: React.FC<LinkButtonProps> = ({ payStatus, payUrl }) => {
  if (payStatus !== 'NEW') return null;

  return (
    <Grid item>
      <Box mr={2}>
        <Link href={payUrl}>
          <Button variant='outlined'>
            <FormattedMessage id='pay' />
          </Button>
        </Link>
      </Box>
    </Grid>
  );
};
