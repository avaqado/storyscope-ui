import React from 'react';
import { AppContainer } from '../app/AppContainer';
import { Formik, FormikActions } from 'formik';
import { PaymentValues } from '../types';
import { handleFormError } from '../utils/handleResponse';
import { useIntl } from 'react-intl';
import { PaymentApi as Api } from '../api/paymentApi';
import { PaymentForm } from './PaymentForm';
import { PaymentUpdateSchema } from '../validations/PaymentUpdate';
import { AxiosResponse, AxiosError } from 'axios';

type ILink = { link: string };

export const PaymentAdd = () => (
  <AppContainer title='paymentAddTitle'>
    <AddForm />
  </AppContainer>
);

const AddForm = () => {
  const intl = useIntl();
  const lang = intl.locale === 'ru-RU' ? 'ru' : 'en';

  const initialPayment: PaymentValues = {
    amount: 999,
    lang
  };

  const handleSubmit = async (
    values: PaymentValues,
    actions: FormikActions<PaymentValues>
  ) => {
    Api.create(values)
      .then(({ data }: AxiosResponse) => redirect(data))
      .catch((e: AxiosError) => handleFormError(e, actions))
      .finally(() => actions.setSubmitting(false));
  };

  return (
    <Formik
      initialValues={initialPayment}
      onSubmit={handleSubmit}
      validationSchema={PaymentUpdateSchema(intl)}
      render={props => <PaymentForm {...props} />}
    />
  );
};

function redirect(data: ILink) {
  window.location.href = data.link;
}
