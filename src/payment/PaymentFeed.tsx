import React from 'react';
import { FormattedMessage } from 'react-intl';
import { useApi } from 'hooks/useApi';
import { Loading } from '../components/Loading';
import { Typography, Box } from '@material-ui/core';
import { Payment } from './Payment';
import { PaymentProps } from '../types';
import { PAYMENTS } from 'api/routes';

export const PaymentFeed = () => {
  const { data: payments, error, isLoading } = useApi(PAYMENTS.ALL);

  if (isLoading) return <Loading />;
  if (error) return <>{error}</>;

  return (
    <Box mt={2}>
      {payments.length ? (
        payments.map((payment: PaymentProps) => (
          <Payment key={payment._id} {...payment} />
        ))
      ) : (
        <Typography variant='subtitle1'>
          <FormattedMessage id='noPayments' />
        </Typography>
      )}
    </Box>
  );
};
