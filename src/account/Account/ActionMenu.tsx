import React, { useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import { MoreVert } from '@material-ui/icons';
import { FormattedMessage } from 'react-intl';
import { useAccountActions } from 'hooks';
import {
  createStyles,
  Theme,
  IconButton,
  Menu,
  MenuItem
} from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    expand: {
      marginLeft: 'auto'
    }
  })
);

export const AccountMenu: React.FC = () => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const { remove, auth } = useAccountActions();
  const classes = useStyles();

  function handleClick(event: React.MouseEvent<HTMLButtonElement>) {
    setAnchorEl(event.currentTarget);
  }

  const handleClose = () => setAnchorEl(null);

  return (
    <>
      <IconButton
        onClick={handleClick}
        className={classes.expand}
        aria-label='more'
      >
        <MoreVert />
      </IconButton>

      <Menu
        open={Boolean(anchorEl)}
        keepMounted
        id='account-menu'
        anchorEl={anchorEl}
        onClose={handleClose}
        onClick={handleClose}
        getContentAnchorEl={null}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center'
        }}
        transformOrigin={{
          vertical: 'bottom',
          horizontal: 'right'
        }}
      >
        <MenuItem dense onClick={auth}>
          <FormattedMessage id='authAgain' />
        </MenuItem>

        <MenuItem dense onClick={auth}>
          <FormattedMessage id='device' />
        </MenuItem>

        <MenuItem dense onClick={remove}>
          <FormattedMessage id='remove' />
        </MenuItem>
      </Menu>
    </>
  );
};
