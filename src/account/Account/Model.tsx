import React from 'react';
import { Details, AccessTime, Comment } from '@material-ui/icons';
import { FormattedMessage, FormattedTime } from 'react-intl';
import { Avatar } from '../../components/Avatar';
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';
import {
  ListItem,
  ListItemIcon,
  ListItemText,
  Card,
  CardHeader,
  Divider,
  List,
  makeStyles,
  Theme,
  createStyles
} from '@material-ui/core';
import { useAccount } from 'hooks';
import { Actions } from './Actions';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    card: {
      marginBottom: theme.spacing(3)
    },
    expand: {
      marginLeft: 'auto'
    },
    verified: {
      fontSize: 15,
      marginLeft: theme.spacing(1)
    }
  })
);

export const Model: React.FC = () => {
  const classes = useStyles();
  const [account] = useAccount();

  const { username, data, status, newbie, expirationDate, comment } = account;

  return (
    <Card className={classes.card}>
      <CardHeader
        avatar={
          <Avatar username={username} src={data && data.profile_pic_url} />
        }
        title={
          <>
            {username}
            {data && data.is_verified && (
              <VerifiedUserIcon className={classes.verified}></VerifiedUserIcon>
            )}
          </>
        }
        subheader={data && data.full_name}
        titleTypographyProps={{
          variant: 'button',
          noWrap: true
        }}
      />

      <Divider variant='middle' light />

      <List aria-label='data'>
        <ListItem>
          <ListItemIcon>
            <Details {...statusColor(status)} />
          </ListItemIcon>
          <ListItemText
            primary={<FormattedMessage id={status} />}
            secondary={<FormattedMessage id='accountStatus' />}
          />
        </ListItem>

        {!newbie && (
          <ListItem>
            <ListItemIcon>
              <AccessTime />
            </ListItemIcon>
            <ListItemText
              secondary={<FormattedMessage id='accountExpirationDate' />}
              primary={
                <FormattedTime
                  value={expirationDate}
                  year='numeric'
                  month='long'
                  day='numeric'
                />
              }
            />
          </ListItem>
        )}

        {comment && (
          <ListItem>
            <ListItemIcon>
              <Comment />
            </ListItemIcon>
            <ListItemText
              primary={comment}
              secondary={<FormattedMessage id='accountComment' />}
            />
          </ListItem>
        )}
      </List>

      <Divider variant='middle' light />

      <Actions />
    </Card>
  );
};

function statusColor(status: string) {
  const colorSchema: any = {
    created: { color: 'secondary' },
    authorized: { color: 'inherit' },
    'code checking': { color: 'primary' },
    'need confirmation': { color: 'primary' }
  };

  return colorSchema[status] || { color: 'error' };
}
