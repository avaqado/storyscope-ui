import React from "react";
import { Button, CardActions } from "@material-ui/core";
import { FormattedMessage } from "react-intl";
import { AccountMenu } from "./ActionMenu";
import { useAccount, useAccountActions } from "hooks";
import { AccountProps } from "types";
import { Link } from "react-router-dom";

export const Actions: React.FC = () => {
  const [account] = useAccount();
  const { code } = useAccountActions();
  const { _id: id, newbie } = account;
  const showCodeAction = needCodeAction(account);

  return (
    <CardActions>
      {showCodeAction ? (
        <Button onClick={code} size="small" color="primary">
          <FormattedMessage id={"accountEnterCode"} />
        </Button>
      ) : (
        <Button
          component={Link}
          to={`/accounts/${id}/extend`}
          size="small"
          color="primary"
        >
          <FormattedMessage id={newbie ? "activate" : "extend"} />
        </Button>
      )}

      <Button
        component={Link}
        to={`/accounts/${id}/edit`}
        size="small"
        color="primary"
      >
        <FormattedMessage id="edit" />
      </Button>

      <AccountMenu />
    </CardActions>
  );
};

function needCodeAction(account: AccountProps) {
  switch (account.status) {
    case "need confirmation":
      return true;
    case "turn off 2fa":
      return true;
    default:
      return false;
  }
}
