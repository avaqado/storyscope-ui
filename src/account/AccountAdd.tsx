import React, { useState } from "react";
import { AppContainer } from "../app/AppContainer";
import { Formik, FormikActions } from "formik";
import { AccountUpdateSchema } from "../validations/account.update";
import { EditAccountValues as AccountValues } from "../types";
import { AccountApi } from "../api";
import { AccountForm } from "./AccountForm";
import { Redirect } from "react-router";
import { handleFormError } from "../utils/handleResponse";
import { useIntl } from "react-intl";
import { ACCOUNTS } from "../api/routes";

export const AccountAdd = () => (
  <AppContainer title="accountAddTitle">
    <NewForm />
  </AppContainer>
);

const NewForm = () => {
  const intl = useIntl();
  const [needRedirect, setRedirect] = useState(false);
  const [account] = useState<AccountValues>({
    username: "",
    password: "",
    comment: "",
    proxy: "",
    user: ""
  });

  const handleSubmit = (
    values: AccountValues,
    actions: FormikActions<AccountValues>
  ) => {
    AccountApi.create(values)
      .then(() => setRedirect(true))
      .catch(error => handleFormError(error, actions))
      .finally(() => actions.setSubmitting(false));
  };

  if (needRedirect) return <Redirect to={ACCOUNTS.ALL} />;

  return (
    <Formik
      initialValues={account}
      onSubmit={handleSubmit}
      validationSchema={AccountUpdateSchema(intl)}
      render={props => <AccountForm {...props} />}
    />
  );
};
