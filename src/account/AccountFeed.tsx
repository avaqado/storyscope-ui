import React from "react";
import AddIcon from "@material-ui/icons/Add";
import { Fab } from "../components/Fab";
import { AccountSkeleton } from "./AccountSkeleton";
import { AccountProps } from "../types";
import { AppContainer } from "../app/AppContainer";
import { Redirect } from "react-router-dom";
import { ACCOUNTS } from "../api/routes";
import { SubtitleLink } from "../components/SubtitleLink";
import { AccountProvider } from "context/account";
import { useApi } from "hooks";

export const AccountFeed = () => {
  const { data: accounts, error, isLoading } = useApi(ACCOUNTS.ALL);

  if (error) return <>{error}</>;
  if (!isLoading && !accounts.length) return <Redirect to="accounts/new" />;

  return (
    <AppContainer
      title="accounts"
      subtitle={<SubtitleLink to={ACCOUNTS.ADD} title="accountAddTitle" />}
    >
      {isLoading ? (
        <>
          <AccountSkeleton />
          <AccountSkeleton />
          <AccountSkeleton />
        </>
      ) : (
        accounts.map((account: AccountProps) => (
          <AccountProvider key={account._id} initialAccount={account} />
        ))
      )}

      <Fab link={ACCOUNTS.ADD}>
        <AddIcon />
      </Fab>
    </AppContainer>
  );
};
