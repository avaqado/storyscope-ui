import React, { useState } from "react";
import { makeStyles } from "@material-ui/styles";
import { Theme, Button, MenuItem } from "@material-ui/core";
import { RouteComponentProps, Redirect } from "react-router";
import { AppContainer } from "../app/AppContainer";
import { AccountApi } from "../api/accountApi";
import { handleFormError } from "../utils/handleResponse";
import { AccountExtendSchema } from "../validations/account.extend";
import { TextField } from "formik-material-ui";
import { Formik, FormikActions, FormikProps, Form, Field } from "formik";
import { FormattedMessage } from "react-intl";
import { useAuth } from "context/auth";
import { AxiosResponse } from "axios";
import { UserValues } from "types";

const useStyles = makeStyles((theme: Theme) => ({
  form: {
    width: "100%",
    marginTop: theme.spacing(2)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

interface ExtendAccountProps extends RouteComponentProps<MatchParams> {}

interface MatchParams {
  id: string;
}

interface ExtendValues {
  term: number;
}

export const AccountExtend: React.FC<ExtendAccountProps> = ({ match }) => (
  <AppContainer title="accountExtendTitle">
    <ExtendForm id={match.params.id} />
  </AppContainer>
);

const ExtendForm: React.FC<MatchParams> = ({ id }) => {
  const [needRedirect, setRedirect] = useState(false);
  const formValues = { term: 30 };
  const { setUser } = useAuth();

  const handleSubmit = (
    values: ExtendValues,
    actions: FormikActions<ExtendValues>
  ) =>
    AccountApi.extend(id, values.term)
      .then(({ data }: AxiosResponse) =>
        setUser((prevState: UserValues) => ({
          ...prevState,
          balance: data.balance
        }))
      )
      .then(() => setRedirect(true))
      .catch(error => handleFormError(error, actions))
      .finally(() => actions.setSubmitting(false));

  if (needRedirect) return <Redirect to="/accounts" />;

  return (
    <Formik
      initialValues={formValues}
      validationSchema={AccountExtendSchema}
      render={props => <InnerForm {...props} />}
      onSubmit={handleSubmit}
    />
  );
};

function InnerForm(props: FormikProps<ExtendValues>) {
  const classes = useStyles();
  const { isSubmitting } = props;

  const options = [
    { value: 30, label: <FormattedMessage id="1 month" /> },
    { value: 7, label: <FormattedMessage id="1 week" /> }
  ];

  return (
    <Form className={classes.form}>
      <Field
        component={TextField}
        name="term"
        id="term"
        label={<FormattedMessage id="term" />}
        variant="outlined"
        margin="normal"
        fullWidth
        select
      >
        {options.map(option => (
          <MenuItem key={option.value} value={option.value}>
            {option.label}
          </MenuItem>
        ))}
      </Field>

      <Button
        type="submit"
        variant="contained"
        color="primary"
        size="large"
        disabled={isSubmitting}
        className={classes.submit}
      >
        <FormattedMessage id="extend" />
      </Button>
    </Form>
  );
}
