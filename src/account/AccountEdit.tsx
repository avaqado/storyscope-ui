import React, { useEffect, useState } from "react";
import axios from "axios";
import { RouteComponentProps, Redirect } from "react-router";
import { AppContainer } from "../app/AppContainer";
import { AccountApi } from "../api/accountApi";
import { AccountUpdateSchema } from "../validations/account.update";
import { Formik, FormikActions } from "formik";
import { handleFormError } from "../utils/handleResponse";
import { EditAccountValues as EditValues, IdMatchParams } from "../types";
import { AccountForm } from "./AccountForm";
import { useIntl } from "react-intl";
import { Loading } from "../components/Loading";

type AccountState = EditValues | null;
interface AccountEditProps extends RouteComponentProps<IdMatchParams> {}

export const AccountEdit: React.FC<AccountEditProps> = ({ match }) => (
  <AppContainer title="accountEditTitle">
    <EditForm id={match.params.id} />
  </AppContainer>
);

const EditForm: React.FC<IdMatchParams> = ({ id }) => {
  const intl = useIntl();
  const [isLoading, setLoading] = useState(true);
  const [errorMessage, setError] = useState(null);
  const [needRedirect, setRedirect] = useState(false);
  const [account, setAccount] = useState<EditValues>({
    username: "",
    password: "",
    comment: "",
    proxy: null,
    user: ""
  });

  const handleSubmit = (
    values: EditValues,
    actions: FormikActions<EditValues>
  ) => {
    AccountApi.update(id, values)
      .then(() => setRedirect(true))
      .catch(error => handleFormError(error, actions))
      .finally(() => actions.setSubmitting(false));
  };

  useEffect(() => {
    let unmounted = false;
    let source = axios.CancelToken.source();

    AccountApi.getOne(id)
      .then(res => {
        if (unmounted) return;

        const { username, password, comment, proxy, user } = res.data;
        setAccount({ username, password, comment, user, proxy: proxy || "" });
        setLoading(false);
      })
      .catch(e => setError(e.response.status));

    return () => {
      unmounted = true;
      source.cancel("Cancelling in cleanup");
    };
  }, [id]);

  if (errorMessage) return <>{errorMessage}</>;
  if (isLoading) return <Loading />;
  if (needRedirect) return <Redirect to="/accounts" />;

  return (
    <Formik
      initialValues={account}
      validationSchema={AccountUpdateSchema(intl)}
      render={props => <AccountForm {...props} />}
      onSubmit={handleSubmit}
    />
  );
};
