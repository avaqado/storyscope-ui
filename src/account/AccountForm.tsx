import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { ProxyApi } from '../api/proxyApi';
import { FormikProps, Form, Field } from 'formik';
import { FormattedMessage } from 'react-intl';
import { EditAccountValues } from '../types';
import { TextField } from 'formik-material-ui';
import { LoadingButton } from '../components/LoadingButton';
import { Loading } from '../components/Loading';
import {
  MenuItem,
  makeStyles,
  Theme,
  InputAdornment,
  IconButton
} from '@material-ui/core';
import { Visibility, VisibilityOff } from '@material-ui/icons';

const useStyles = makeStyles((theme: Theme) => ({
  form: {
    width: '100%',
    marginTop: theme.spacing(2)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

export const AccountForm = (props: FormikProps<EditAccountValues>) => {
  const { isSubmitting } = props;
  const classes = useStyles();

  const [hidden, setHidden] = useState(true);
  const [isLoading, setLoading] = useState(false);
  const [errorMessage, setError] = useState(null);
  const [proxies, setProxies] = useState<any[]>([]);

  const handleClickShowPassword = () => setHidden(prev => !prev);

  useEffect(() => {
    let unmounted = false;
    let source = axios.CancelToken.source();

    setLoading(true);

    setError(null);
    ProxyApi.all()
      .then(res => !unmounted && setProxies(res.data))
      .then(() => !unmounted && setLoading(false));

    return () => {
      unmounted = true;
      source.cancel('Cancelling in cleanup');
    };
  }, []);

  if (errorMessage) return <>{errorMessage}</>;
  if (isLoading) return <Loading />;

  return (
    <Form className={classes.form}>
      <Field
        label={<FormattedMessage id='username' />}
        component={TextField}
        name='username'
        variant='outlined'
        autoComplete='nope'
        margin='normal'
        id='username'
        required
        fullWidth
      />

      <Field
        label={<FormattedMessage id='password' />}
        component={TextField}
        name='password'
        variant='outlined'
        autoComplete='new-password'
        margin='normal'
        type={hidden ? 'password' : 'text'}
        id='password'
        required
        fullWidth
        InputProps={{
          endAdornment: (
            <InputAdornment position='end'>
              <IconButton
                aria-label='toggle password visibility'
                onClick={handleClickShowPassword}
                // onMouseDown={handleMouseDownPassword}
              >
                {hidden ? <VisibilityOff /> : <Visibility />}
              </IconButton>
            </InputAdornment>
          )
        }}
      />

      <Field
        label={<FormattedMessage id='comment' />}
        component={TextField}
        name='comment'
        variant='outlined'
        margin='normal'
        type='text'
        id='comment'
        fullWidth
      />

      <Field
        label={<FormattedMessage id='proxy' />}
        name='proxy'
        id='proxy'
        component={TextField}
        variant='outlined'
        margin='normal'
        fullWidth
        select
      >
        {proxies.map(option => (
          <MenuItem key={option._id} value={option._id}>
            {option.host}
          </MenuItem>
        ))}
      </Field>

      <LoadingButton title='save' size='large' isLoading={isSubmitting} />
    </Form>
  );
};
