import { object, string } from 'yup';

export const SigninSchema = (intl: any) =>
  object().shape({
    username: string()
      .email(intl.formatMessage({ id: 'invalidEmail' }))
      .required(intl.formatMessage({ id: 'emailRequired' })),

    password: string().required(intl.formatMessage({ id: 'passwordRequired' }))
  });
