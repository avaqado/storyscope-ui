import { object, string } from 'yup';

export const PasswordResetSchema = (intl: any) =>
  object().shape({
    username: string()
      .email(intl.formatMessage({ id: 'invalidEmail' }))
      .required(intl.formatMessage({ id: 'emailRequired' }))
  });
