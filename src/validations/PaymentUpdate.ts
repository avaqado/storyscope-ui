import { object, number } from 'yup';

export const PaymentUpdateSchema = (intl: any) =>
  object().shape({
    amount: number()
      .required(intl.formatMessage({ id: 'sumRequired' }))
      .min(1)
  });
