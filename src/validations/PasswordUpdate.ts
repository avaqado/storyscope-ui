import { object, string, ref } from 'yup';

export const PasswordUpdateSchema = (intl: any) =>
  object().shape({
    password: string().required(intl.formatMessage({ id: 'passwordRequired' })),

    confirmPassword: string()
      .required(intl.formatMessage({ id: 'passwordRequired' }))
      .oneOf(
        [ref('password'), null],
        intl.formatMessage({ id: 'passwordsMustMatch' })
      )
  });
