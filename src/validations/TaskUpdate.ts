import { object, string, mixed, number } from 'yup';

export const TaskUpdateSchema = (intl: any) =>
  object().shape({
    name: string().required(),
    account: string().required(),
    list: string().required(),
    parseSpeed: mixed().oneOf(['fast', 'middle', 'slow', 'ultra', 'flash']),
    donorLimit: number()
      .min(0)
      .required(),
    seePerUser: number()
      .min(1)
      .max(8)
  });
