import { object, string } from 'yup';

export const AccountUpdateSchema = (intl: any) =>
  object().shape({
    username: string().required(
      intl.formatMessage({ id: 'accountUsernameRequired' })
    ),
    password: string().required(
      intl.formatMessage({ id: 'accountPasswordRequired' })
    ),
    comment: string().max(
      30,
      intl.formatMessage({ id: 'accountCommentTooLong' })
    )
    // proxy: string().required(intl.formatMessage({ id: 'accountProxyRequired' }))
  });
