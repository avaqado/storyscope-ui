import { object, number } from 'yup';

export const AccountCodeSchema = (intl: any) =>
  object().shape({
    code: number().required(intl.formatMessage({ id: 'accountCodeRequired' }))
  });
