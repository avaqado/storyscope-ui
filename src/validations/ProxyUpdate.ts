import { object, string } from 'yup';

export const ProxyUpdateSchema = (intl: any) =>
  object().shape({
    host: string().required(intl.formatMessage({ id: 'proxyHostRequired' })),
    username: string().required(
      intl.formatMessage({ id: 'proxyUsernameRequired' })
    ),
    password: string().required(
      intl.formatMessage({ id: 'proxyPasswordRequired' })
    ),
    comment: string().max(30, intl.formatMessage({ id: 'commentTooLong' }))
  });
