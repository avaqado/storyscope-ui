import { object, number } from 'yup';

export const AccountExtendSchema = object().shape({
  term: number().required('term is required')
});
