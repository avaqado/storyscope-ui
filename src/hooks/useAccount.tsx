import { useContext } from "react";
import { AccountContext } from 'context/account'

export const useAccount = () => useContext(AccountContext).slice(0, 2);
export const useAccountActions = () => useContext(AccountContext)[2]