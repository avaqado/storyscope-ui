import { useEffect, useState } from 'react';
import axios, { AxiosResponse, Method, AxiosError } from 'axios';
import { api } from 'api/config';

export const useApi = (
  url: string,
  method: Method = 'get',
  params?: object
) => {
  const [response, setResponse] = useState<AxiosResponse | null>(null);
  const [error, setError] = useState<AxiosError | null>(null);
  const [isLoading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    let mounted = true;
    let source = axios.CancelToken.source();
    // setLoading(true);

    api({ url, method, params })
      .then((resp: AxiosResponse) => mounted && setResponse(resp))
      .catch((err: AxiosError) => mounted && setError(err))
      .finally(() => mounted && setLoading(false));

    return () => {
      mounted = false;
      source.cancel();
    };
  }, [url, method, params]);

  const data = response && response.data;

  return { response, error, isLoading, data };
};

export const usePost = (url: string, params: object) =>
  useApi(url, 'post', params);

export const useGet = (url: string, params: object) =>
  useApi(url, 'get', params);
