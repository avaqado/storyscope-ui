import { useState, useEffect } from 'react';
import axios from 'axios';

export const useFetch = (request: any, trash: string) => {
  const [data, setData] = useState();
  const [isLoading, setLoading] = useState<boolean>(true);
  const [errorMessage, setError] = useState<string | null>(null);

  useEffect(() => {
    let mounted = true;
    let source = axios.CancelToken.source();

    request()
      .then((res:any) => mounted && setData(res.data))
      .catch((e:any) => mounted && setError(e.message))
      .finally(() => setLoading(false));

    return () => {
      mounted = false;
      source.cancel('Cancelling in cleanup');
    };
  }, []);

  return { data, isLoading, errorMessage };
};
