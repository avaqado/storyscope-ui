import { useContext } from "react";
import { TaskContext } from 'context/task'


export const useTask = () => useContext(TaskContext).slice(0, 2);

export const useTaskActions = () => useContext(TaskContext)[2]