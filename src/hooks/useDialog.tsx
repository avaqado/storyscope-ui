import { DialogContext } from '../context/formDialog';
import { useContext } from 'react';

export const useDialog = () => useContext(DialogContext);

export const useConfirmDialog = () => useContext(DialogContext).slice(2, 4);

export const useFormDialog = () => useContext(DialogContext).slice(0, 2);
