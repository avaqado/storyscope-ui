import { ACCOUNTS } from './routes';
import { api } from './config';
import { CodeValues } from '../types';

const all = () => api.get(ACCOUNTS.ALL);

const getOne = (id: string) => api.get(`/accounts/${id}`);

const status = (id: string) => api.get(`/accounts/${id}/status`);

const create = (params: object) => api.post('/accounts', params);

const remove = (id: string) => api.delete(`/accounts/${id}`);

const update = (id: string, params: object) =>
  api.patch(`/accounts/${id}`, params);

const auth = (id: string) => api.post(`/accounts/${id}/auth`);

const confirm = (id: string, values: CodeValues) =>
  api.post(`/accounts/${id}/confirm`, values);

const extend = (id: string, term: number) =>
  api.post(`/accounts/${id}/extend`, { term });

export const AccountApi = {
  all,
  getOne,
  update,
  create,
  extend,
  remove,
  status,
  confirm,
  auth
};
