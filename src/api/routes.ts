export const SESSION = {
  ME: '/me',
  SIGN_UP: '/auth/register',
  SIGN_IN: '/auth',
  SIGN_OUT: '/auth',
  FORGOT: '/auth/forgot',
  FORGOT_SUCCESS: '/auth/forgot/succes',
  SEND_TOKEN: '/auth/reset',
  RESET: '/auth/reset/:token',
  CHECK_TOKEN: '/auth/reset/check',
  UPDATE_PASSWORD: '/auth/reset/update'
};

export const ACCOUNTS = {
  ADD: '/accounts/new',
  ALL: '/accounts',
  ONE: '/accounts/:id',
  EDIT: '/accounts/:id/edit',
  EXTEND: '/accounts/:id/extend'
};

export const PROXIES = {
  ALL: '/proxies',
  ADD: '/proxies/new',
  EDIT: '/proxies/:id/edit'
};

export const TASKS = {
  ALL: '/tasks',
  ACTIVE: '/tasks/active',
  ADD: '/tasks/new',
  EDIT: '/tasks/:id/edit',
  STATS: '/tasks/:id/stats',
  RECORDS: '/tasks/:id/records'
};

export const PAYMENTS = {
  ALL: '/payments',
  ADD: '/payments/new',
  INIT_PAYMENT: 'https://securepay.tinkoff.ru/v2/Init'
};

export const PROFILE = {
  INDEX: '/profile',
  REFS: '/referrals',
  TELEGRAM: '/telegram'
};

export const USERS = {
  ALL: '/users',
  REFS: '/referrals',
  TELEGRAM: '/telegram'
};
