import { api } from './config';

const all = () => api.get('/tasks');

const create = (params: object) => api.post('/tasks', params);

const getOne = (id: string) => api.get(`/tasks/${id}`);

const info = (id: string) => api.get(`/tasks/${id}/info`);

const destroy = (id: string) => api.delete(`/tasks/${id}`);

const getAccounts = () => api.get('/accounts');

const restart = (id: string) => api.post(`/tasks/${id}/restart`);

const resume = (id: string) => api.post(`/tasks/${id}/resume`);

const stop = (id: string) => api.post(`/tasks/${id}/stop`);

const stats = (id: string) => api.get(`/tasks/${id}/stats`);

const records = (id: string, params: object) =>
  api.get(`/tasks/${id}/records`, params);

const update = (id: string, params: object) =>
  api.patch(`/tasks/${id}`, params);

export const TaskApi = {
  all,
  create,
  getOne,
  update,
  destroy,
  restart,
  resume,
  records,
  stop,
  stats,
  info,
  getAccounts
};
