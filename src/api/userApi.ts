import { api } from './config';

const update = (id: string, params: object) =>
  api.patch(`/users/${id}`, params);

const telegram = (params: object) => api.post('/users/telegram', params);

export const UserApi = {
  update,
  telegram
};
