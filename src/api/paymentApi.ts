import axios from 'axios';
import { api } from './config';
import { PaymentValues, InitRequest, InitResponse } from '../types';
import { PAYMENTS } from './routes';

const all = () => api.get('/payments');

const create = (params: PaymentValues) => api.post('/payments', params);

const init = (params: InitRequest) => axios.post(PAYMENTS.INIT_PAYMENT, params);

const getOne = (id: string) => api.get(`/tasks/${id}`);

const destroy = (id: string) => api.delete(`/tasks/${id}`);

const getAccounts = () => api.get('/accounts');

const update = (id: string, params: object) =>
  api.patch(`/payments/${id}`, params);

const setLink = ({ OrderId: id, PaymentURL: link }: InitResponse) =>
  api.patch(`payments/${id}/link`, { link });

export const PaymentApi = {
  all,
  init,
  create,
  getOne,
  update,
  destroy,
  setLink,
  getAccounts
};
