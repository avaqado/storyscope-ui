import { PROXIES } from './routes';
import { api } from './config';
import { InitRequest } from '../types';

const all = (user?: string) => api.get(PROXIES.ALL, { params: { user: user } });

const create = (params: object) => api.post('/proxies', params);

const getOne = (id: string) => api.get(`/proxies/${id}`);

const destroy = (id: string) => api.delete(`/proxies/${id}`);

const update = (id: string, params: object) =>
  api.patch(`/proxies/${id}`, params);

const init = (data: InitRequest) =>
  api.post('https://securepay.tinkoff.ru/v2/Init', data);

export const ProxyApi = {
  all,
  init,
  create,
  getOne,
  update,
  destroy
};
