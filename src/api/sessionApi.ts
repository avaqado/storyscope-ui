import { UserAuth, ResetProps, ResetPasswordValues } from '../types';
import { SESSION } from './routes';
import { api } from './config';

const me = () => api.get(SESSION.ME);

const signin = (data: UserAuth) => api.post(SESSION.SIGN_IN, data);

const signup = (data: UserAuth) => api.post(SESSION.SIGN_UP, data);

const reset = (data: ResetProps) => api.post(SESSION.SEND_TOKEN, data);

const signout = () => api.delete(SESSION.SIGN_OUT);

const checkResetToken = (token: string) =>
  api.post(SESSION.CHECK_TOKEN, { token });

const updatePassword = (data: ResetPasswordValues) =>
  api.post(SESSION.UPDATE_PASSWORD, data);

export const sessionApi = {
  checkResetToken,
  updatePassword,
  signin,
  signup,
  signout,
  reset,
  me
};
