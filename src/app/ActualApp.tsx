import React, { useEffect } from 'react';
import { useAuth } from '../context/auth';

const loadUserApp = () => import('./UserApp');
const loadGuestApp = () => import('./GuestApp');

const UserApp = React.lazy(loadUserApp);
const GuestApp = React.lazy(loadGuestApp);

export const ActualApp = () => {
  const { user } = useAuth();

  // pre-load app in the background
  useEffect(() => {
    loadUserApp();
  }, []);

  return user ? <UserApp /> : <GuestApp />;
};
