import clsx from 'clsx';
import React, { useState } from 'react';
import { useLocalStorage } from '../hooks/useLocalStorage';
import { Route, Redirect, Switch } from 'react-router';
import { Header } from '../components/Header';
import { AccountFeed } from '../account/AccountFeed';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { AccountEdit } from '../account/AccountEdit';
import { AccountExtend } from '../account/AccountExtend';
import { AccountAdd } from '../account/AccountAdd';
import { ProxyFeed } from '../proxy/ProxyFeed';
import { ProxyEdit } from '../proxy/ProxyEdit';
import { ProxyAdd } from '../proxy/ProxyAdd';
import { TaskAdd } from '../task/TaskAdd';
import { taskEdit } from '../task/TaskEdit';
import { TaskStats } from '../task/TaskStats';
import { TaskRecords } from '../task/TaskRecords';
import { ProfilePage } from '../profile/Page';
import { Referrals } from '../profile/Referrals';
import { TelegramAdd } from '../profile/TelegramAdd';
import { PaymentAdd } from '../payment/PaymentAdd';
import { CssBaseline } from '@material-ui/core';
import { UserFeed } from 'admin/UserFeed';
import { UserTaskFeed } from 'task/UserTaskFeed';
import { ActiveTaskFeed } from 'task/ActiveTaskFeed';
import {
  ACCOUNTS,
  PROXIES,
  TASKS,
  PAYMENTS,
  PROFILE,
  USERS
} from '../api/routes';

const drawerWidth = 240;
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    grow: {
      flexGrow: 1
    },
    root: {
      display: 'flex'
    },
    container: {
      [theme.breakpoints.down('xs')]: {
        padding: theme.spacing(0)
      }
    },
    title: {
      marginBottom: theme.spacing(3)
    },
    drawer: {
      [theme.breakpoints.up('sm')]: {
        width: drawerWidth,
        flexShrink: 0
      }
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      })
    },
    desktopMenuButton: {
      marginLeft: theme.spacing(-2),
      marginRight: theme.spacing(2),
      [theme.breakpoints.down('xs')]: {
        display: 'none'
      }
    },
    menuButton: {
      marginRight: theme.spacing(0),
      [theme.breakpoints.up('sm')]: {
        display: 'none'
      }
    },
    drawerIcon: {
      marginLeft: theme.spacing(1)
    },
    drawerItem: {
      marginLeft: theme.spacing(0),
      paddingLeft: theme.spacing(0)
    },
    drawerPaper: {
      width: drawerWidth
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      }),
      [theme.breakpoints.up('sm')]: {
        marginLeft: -drawerWidth
      }
    },
    contentShift: {
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen
      }),
      marginLeft: 0
    },
    divider: {
      marginTop: 10,
      marginBottom: 10
    },
    toolbar: theme.mixins.toolbar
  })
);

export const AppLayout = () => {
  const classes = useStyles();

  const [open, setOpen] = useLocalStorage('drawerOpen', true);
  const [mobileOpen, setMobileOpen] = useState(false);

  const handleDrawerToggle = () => setOpen(!open);
  const handleMobileDrawerToggle = () => setMobileOpen(!mobileOpen);

  return (
    <div className={classes.root}>
      <CssBaseline />

      <Header
        open={open}
        mobileOpen={mobileOpen}
        desktopOnClick={handleDrawerToggle}
        mobileOnClick={handleMobileDrawerToggle}
      />

      <main className={clsx(classes.content, { [classes.contentShift]: open })}>
        <div className={classes.toolbar} />
        <Switch>
          <Route exact path='/' component={UserTaskFeed} />
          <Route exact path={TASKS.ALL} component={UserTaskFeed} />
          <Route exact path={TASKS.EDIT} component={taskEdit} />
          <Route exact path={TASKS.ADD} component={TaskAdd} />
          <Route exact path={TASKS.STATS} component={TaskStats} />
          <Route exact path={TASKS.RECORDS} component={TaskRecords} />
          <Route exact path={TASKS.ACTIVE} component={ActiveTaskFeed} />

          <Route exact path={ACCOUNTS.ALL} component={AccountFeed} />
          <Route exact path={ACCOUNTS.EDIT} component={AccountEdit} />
          <Route exact path={ACCOUNTS.EXTEND} component={AccountExtend} />
          <Route exact path={ACCOUNTS.ADD} component={AccountAdd} />

          <Route exact path={PAYMENTS.ADD} component={PaymentAdd} />

          <Route exact path={PROFILE.INDEX} component={ProfilePage} />
          <Route exact path={PROFILE.REFS} component={Referrals} />
          <Route exact path={PROFILE.TELEGRAM} component={TelegramAdd} />

          <Route exact path={PROXIES.ALL} component={ProxyFeed} />
          <Route exact path={PROXIES.EDIT} component={ProxyEdit} />
          <Route exact path={PROXIES.ADD} component={ProxyAdd} />

          <Route exact path={USERS.ALL} component={UserFeed} />

          <Route render={() => <Redirect to='/' />} />
        </Switch>
      </main>
    </div>
  );
};
