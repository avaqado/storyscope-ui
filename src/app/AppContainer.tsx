import React from 'react';
import { Container, Typography } from '@material-ui/core';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { FormattedMessage } from 'react-intl';

interface IContainer {
  title: string;
  subtitle?: any;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      [theme.breakpoints.down('xs')]: {
        padding: theme.spacing(0)
      }
    },
    title: {
      marginBottom: theme.spacing(3)
    }
  })
);

export const AppContainer: React.FC<IContainer> = ({
  title,
  subtitle,
  children
}) => {
  const classes = useStyles();

  return (
    <Container maxWidth='lg' className={classes.container}>
      <Typography variant='h5' component='h5'>
        <FormattedMessage id={title} />
      </Typography>
      {subtitle}
      {children}
    </Container>
  );
};
