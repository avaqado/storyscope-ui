import React from 'react';
import { LoginPage } from '../pages/LoginPage';
import { Route, Switch, Redirect } from 'react-router';
import { LandingPage } from '../pages/LandingPage';
import { RegisterPage } from '../pages/RegisterPage';
import { ForgotPage } from '../pages/ForgotPage';
import { SESSION } from '../api/routes';
import { ForgotSuccessPage } from '../pages/ForgotSuccess.Page';
import { ResetPasswordPage } from '../pages/ResetPasswordPage';

const AppGuest = () => (
  <Switch>
    <Route path='/' component={LandingPage} exact />
    <Route path={SESSION.SIGN_IN} component={LoginPage} exact />
    <Route path={SESSION.SIGN_UP} component={RegisterPage} exact />
    <Route path={SESSION.FORGOT} component={ForgotPage} exact />
    <Route path={SESSION.RESET} component={ResetPasswordPage} exact />
    <Route path={SESSION.FORGOT_SUCCESS} component={ForgotSuccessPage} exact />
    <Route render={() => <Redirect to={'/'} />} />
  </Switch>
);

export default AppGuest;
