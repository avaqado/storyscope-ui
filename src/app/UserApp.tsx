import React from 'react';
import { AppLayout } from './UserAppLayout';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import { grey } from '@material-ui/core/colors';
import { SnackBarProvider } from '../context/snackbar';
import { DialogProvider } from '../context/formDialog';
import { ErrorBoundary } from 'components/ErrorBoundary';

const theme = createMuiTheme({
  palette: {
    primary: { main: grey[900] },
    secondary: { main: '#11cb5f' }
  }
});

const App = () => (
  <ThemeProvider theme={theme}>
    <SnackBarProvider>
      <DialogProvider>
        <ErrorBoundary>
          <AppLayout />
        </ErrorBoundary>
      </DialogProvider>
    </SnackBarProvider>
  </ThemeProvider>
);

export default App;
