import React, { createContext, useState } from 'react';
import { TaskProps } from 'types';
import { Task } from 'task/Task/Model';
import { TaskApi } from 'api';
import { useSnackbar, useConfirmDialog } from 'hooks';
import { handleApiError } from 'utils/handleResponse';
import { AxiosResponse } from 'axios';

export const TaskContext = createContext<any>(null);

interface TaskProviderProps {
  initialTask: TaskProps;
}

export const TaskProvider: React.FC<TaskProviderProps> = ({ initialTask }) => {
  const [task, setTask] = useState<TaskProps>(initialTask);
  const [hide, setHide] = useState<boolean>(false);
  const [menuLoading, setMenuLoading] = useState<boolean>(false);
  const [showConfirm, clearConfirm] = useConfirmDialog();

  const addAlert = useSnackbar();

  const remove = () => {
    const handleConfirm = () => {
      TaskApi.destroy(task._id)
        .then(() => setHide(true))
        .catch(e => {
          handleApiError(e);
          setHide(false);
          addAlert({ messageId: 'error' });
        });

      clearConfirm();
    };

    showConfirm({
      handleConfirm,
      title: 'taskRemoveDialogTitle'
    });
  };

  const info = () => {
    TaskApi.info(task._id)
      .then(({ data }: AxiosResponse<TaskProps>) => setTask(data))
      .catch(error => addAlert({ messageId: error.response.data }));
  };

  const restart = () => {
    setMenuLoading(true);

    TaskApi.restart(task._id)
      .then(({ data }: AxiosResponse<TaskProps>) => setTask(data))
      .catch(e => addAlert({ messageId: e.response.data }))
      .finally(() => setMenuLoading(false));
  };

  const resume = () => {
    setMenuLoading(true);

    TaskApi.resume(task._id)
      .then(({ data }: AxiosResponse<TaskProps>) => setTask(data))
      .catch(e => addAlert({ messageId: e.response.data }))
      .finally(() => setMenuLoading(false));
  };

  const stop = () => {
    setMenuLoading(true);

    TaskApi.stop(task._id)
      .then(({ data }: AxiosResponse<TaskProps>) =>
        setTask(prev => ({ ...prev, status: data.status }))
      )
      .finally(() => setMenuLoading(false));
  };

  const actions = { remove, restart, resume, stop, info, menuLoading };
  if (hide) return null;

  return (
    <TaskContext.Provider value={[task, setTask, actions]}>
      <Task />
    </TaskContext.Provider>
  );
};
