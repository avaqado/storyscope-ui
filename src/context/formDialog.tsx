import React, { createContext, useState } from 'react';
import { FormDialog } from '../components/FormDialog';
import { ConfirmDialog } from '../components/ConfirmDialog';
import { FormDialogProps, ConfirmDialogProps } from '../types';

export const DialogProvider: React.FC = ({ children }) => {
  const [form, setForm] = useState<FormDialogProps | null>();
  const [confirm, setConfirm] = useState<ConfirmDialogProps | null>();

  const showForm = (details: FormDialogProps) => setForm(details);
  const showConfirm = (details: ConfirmDialogProps) => setConfirm(details);

  const clearForm = () => setForm(null);
  const clearConfirm = () => setConfirm(null);

  const value = [showForm, clearForm, showConfirm, clearConfirm];

  return (
    <DialogContext.Provider value={value}>
      {children}
      {form && <FormDialog {...form} handleClose={clearForm} />}
      {confirm && <ConfirmDialog {...confirm} handleClose={clearConfirm} />}
    </DialogContext.Provider>
  );
};

export const DialogContext = createContext<any>(null);
