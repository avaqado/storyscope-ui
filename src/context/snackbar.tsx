import React, { useState, useEffect, createContext } from 'react';
import { Snackbar, Theme } from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/styles';

const AUTO_HIDE = 3000;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    snackbar: {
      [theme.breakpoints.down('xs')]: {
        bottom: 90
      }
    }
  })
);

export const SnackbarContext = createContext<any>(null);

export const SnackBarProvider: React.FC = ({ children }) => {
  const [alerts, setAlerts] = useState<any>([]);
  const activeAlertIds = alerts.join(',');
  const classes = useStyles();

  useEffect(() => {
    if (activeAlertIds.length > 0) {
      const timer = setTimeout(
        () => setAlerts((alerts: any[]) => alerts.slice(0, alerts.length - 1)),
        AUTO_HIDE
      );
      return () => clearTimeout(timer);
    }
  }, [activeAlertIds]);

  const addAlert = (alert: any) =>
    setAlerts((alerts: any[]) => [alert, ...alerts]);

  return (
    <SnackbarContext.Provider value={addAlert}>
      {children}
      {alerts.map((alert: any) => (
        <Snackbar
          open
          className={classes.snackbar}
          key={alert}
          autoHideDuration={AUTO_HIDE}
          anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
          ContentProps={{
            'aria-describedby': 'snackbar-fab-message-id'
          }}
          message={<span id='snackbar-fab-message-id'>{alert.messageId}</span>}
        />
      ))}
    </SnackbarContext.Provider>
  );
};
