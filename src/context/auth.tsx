import React, { useState, useEffect, createContext, useContext } from 'react';
import { Loading } from '../components/Loading';
import { UserAuth, UserValues, ResetPasswordValues } from '../types';
import { sessionApi } from '../api';
import { FormikActions } from 'formik';
import { handleLoginError, handleRegisterError } from '../utils/handleResponse';
import { useIntl } from 'react-intl';

type UserState = UserValues | null;
const AuthContext = createContext<any>(null);

export const useAuth = () => useContext(AuthContext);

export const AuthProvider: React.FC = ({ children }) => {
  const [isLoading, setLoading] = useState<boolean>(true);
  const [user, setUser] = useState<UserState>(null);

  const intl = useIntl();
  const incorrectPassord = intl.formatMessage({ id: 'incorrectPassword' });
  const hasError = intl.formatMessage({ id: 'hasError' });

  const signout = () => {
    sessionApi
      .signout()
      .then(() => setUser(null))
      .catch(e => console.log(e.message));
  };

  const signin = (values: UserAuth) => {
    sessionApi
      .signin(values)
      .then(res => setUser(res.data))
      .catch(e => console.log(e));
  };

  const login = (values: UserAuth, actions: FormikActions<UserAuth>) => {
    sessionApi
      .signin(values)
      .then(res => setUser(res.data))
      .catch(error => handleLoginError(error, actions, incorrectPassord))
      .finally(() => actions.setSubmitting(false));
  };

  const signup = (values: UserAuth, actions: FormikActions<UserAuth>) => {
    sessionApi
      .signup(values)
      .then(res => setUser(res.data))
      .catch(error => handleRegisterError(error, actions))
      .finally(() => actions.setSubmitting(false));
  };

  const updatePassword = (
    values: ResetPasswordValues,
    actions: FormikActions<ResetPasswordValues>
  ) => {
    sessionApi
      .updatePassword(values)
      .then(res => setUser(res.data))
      .catch(error => handleLoginError(error, actions, hasError))
      .finally(() => actions.setSubmitting(false));
  };

  useEffect(() => {
    sessionApi
      .me()
      .then(res => {
        setLoading(false);
        setUser(res.data);
      })
      .catch(e => {
        if (user) setUser(null);
        setLoading(false);
      });
  }, []);

  if (isLoading) return <Loading />;

  return (
    <AuthContext.Provider
      value={{ user, signout, signin, login, signup, setUser, updatePassword }}
    >
      {children}
    </AuthContext.Provider>
  );
};
