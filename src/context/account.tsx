import React, { createContext, useState } from "react";
import { useDialog, Interval, useSnackbar, useInterval } from "hooks";
import { AccountProps, CodeValues } from "types";
import { useIntl } from "react-intl";
import { AccountApi } from "api";
import { AxiosResponse } from "axios";
import { handleApiError, handleFormError } from "utils/handleResponse";
import { FormikActions } from "formik";
import { AccountCodeSchema } from "validations/account.code";
import { Model } from "account/Account/Model";

export const AccountContext = createContext<any>(null);

interface Props {
  initialAccount: AccountProps;
}

export const AccountProvider: React.FC<Props> = ({ initialAccount }) => {
  const [showForm, clearForm, showConfirm, clearConfirm] = useDialog();
  const [account, setAccount] = useState<AccountProps>(initialAccount);
  const initialDelay = statusDelay(account);
  const [delay, setDelay] = useState<Interval>(initialDelay);
  const [hide, setHide] = useState(false);

  const intl = useIntl();
  const addAlert = useSnackbar();

  useInterval(() => {
    AccountApi.getOne(account._id).then(
      ({ data }: AxiosResponse<AccountProps>) => {
        const status = data.status;

        if (status !== account.status) {
          setAccount(data);
          setDelay(null);
        }
      }
    );
  }, delay);

  const auth = async () => {
    await AccountApi.auth(account._id).catch(e => handleApiError(e));
    await AccountApi.getOne(account._id).then(res => setAccount(res.data));
    setDelay(5000);
  };

  const remove = () => {
    const handleConfirm = () => {
      AccountApi.remove(account._id)
        .then(() => setHide(true))
        .catch(error => {
          handleApiError(error);
          setHide(false);
          addAlert({ messageId: error.response.data });
        });

      clearConfirm();
    };

    showConfirm({
      handleConfirm,
      title: "accountRemoveDialogTitle"
    });
  };

  const code = () => {
    const handleSubmit = (
      values: CodeValues,
      actions: FormikActions<CodeValues>
    ) => {
      AccountApi.confirm(account._id, values)
        .then(({ data }: AxiosResponse<AccountProps>) => {
          setAccount(prev => ({ ...prev, status: data.status }));
          setDelay(3000);
          clearForm();
        })
        .catch(error => {
          handleFormError(error, actions);
          actions.setSubmitting(false);
        });
    };

    showForm({
      handleSubmit,
      openDialog: true,
      validationSchema: AccountCodeSchema(intl),
      dialogTitle: "enterCode",
      initialValues: { code: "" },
      fields: [
        {
          name: "code",
          id: "code",
          label: "code"
        }
      ]
    });
  };

  const actions = { code, remove, auth };

  if (hide) return null;

  return (
    <AccountContext.Provider value={[account, setAccount, actions]}>
      <Model />
    </AccountContext.Provider>
  );
};

function statusDelay(account: AccountProps) {
  switch (account.status) {
    case "code checking":
      return 3000;
    case "created":
      return 5000;
    default:
      return null;
  }
}
