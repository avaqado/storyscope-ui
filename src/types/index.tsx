export interface TaskProps {
  account: AccountProps;
  activeProfile: DonorProps;
  _id: string;
  name: string;

  approach: number;
  status: number;

  createdAt: string;
  startedAt: string;
  finishedAt: string;

  restart: boolean;
  seenUniq: boolean;
  needParse: boolean;

  parseSpeed: string;

  donorLimit: number;
  found: number;
  ignoreStoryCount: number;
  lastSeen: number;
  list: [];

  parsedCount: number;
  pendingCount: number;
  profilesCount: number;
  storiesCount: number;
  cycleCount: number;
  duplicatedCount: number;

  seeCount: number;
  seePerUser: number;
  seenCount: number;
  seenCountBefore: number;

  storiesBatch: number;
  storyTouch: number;
  touch: string;
  user: string;
  usernames: [];
  parseSpeedData: { count: number; average: number };
}

export interface UserProps {
  _id: string;
  username: string;
  freePoints: number;
  balance: number;
  refBalance: number;
}

export interface DonorProps {
  _id: string;
  seenCount: number;
  parsedCount: number;
  username: string;
  data: {
    pk: number;
    username: string;
    full_name: string;
    is_private: boolean;
    profile_pic_url: string;
    profile_pic_id: string;
    follower_count: number;
  };
}

export interface PaymentProps {
  _id: string;
  amount: number;
  refunded: number;
  data: any;
  payStatus: PaymentStatus;
  payUrl: string;
}

type PaymentStatus =
  | 'NEW'
  | 'AUTHORIZED'
  | 'CONFIRMED'
  | 'REVERSED'
  | 'REFUNDED'
  | 'PARTIAL_REFUNDED'
  | 'REJECTED';

export interface ResetProps {
  username: string;
}

export interface EditAccountValues {
  username: string;
  password: string;
  comment: string;
  proxy: string | null;
  user: string;
}

export interface ProxyValues {
  host: string;
  username: string;
  password: string;
  comment: string;
  user: string;
}

export interface RecordProps {
  _id: string;
  createdAt: string;
  params: string;
  from: string;
  error: string;
  message: string;
}

export interface TaskValues {
  name: string;
  account: string;
  list: string;
  parseSpeed: string;
  seePerUser: number;
  donorLimit: number;
  restart: boolean;
  seenUniq: boolean;
  onlyRussian: boolean;
  noArab: boolean;
  stopWords: boolean;
  justParse: boolean;
  vote: boolean;
}

export interface ResetPasswordValues {
  password: string;
  confirmPassword: string;
  token: string;
}

export interface UserValues {
  _id: string;
  type: string;
  username: string;
  balance: number;
  refBalance: number;
  freePoints: number;
  telegramId: number;
}

export interface InitResponse {
  Amount: number;
  ErrorCode: string;
  OrderId: string;
  PaymentId: number;
  PaymentURL: string;
  Status: string;
  Success: true;
  TerminalKey: string;
}

export interface InitRequest {
  TerminalKey: string;
  Amount: number;
  OrderId: string;
  Description?: string;
  IP?: string;
  Token?: string;
  CustomerKey?: string;
  Recurrent?: string;
  RedirectDueDate?: string;
  NotificationURL?: string;
  SuccessURL?: string;
  FailURL?: string;
  DATA?: object;
  Receipt?: PaymentReceipt;
  PayType?: 'О' | 'T';
  Language?: 'ru' | 'en';
}

export interface PaymentReceipt {
  Email?: string;
  Phone?: string;
  EmailCompany?: string;
  Items: PaymentReceiptItem[];
  Taxation:
    | 'osn'
    | 'usn_income'
    | 'usn_income_outcome'
    | 'envd'
    | 'esn'
    | 'patent';
}

export interface PaymentReceiptItem {
  Name: string;
  Price: number;
  Amount: number;
  Quantity: number;
  Ean13?: string;
  ShopCode?: string;
  Tax: 'none' | 'vat0' | 'vat10' | 'vat20' | 'vat110' | 'vat120';
  PaymentMethod?:
    | 'full_prepayment'
    | 'prepayment'
    | 'advance '
    | 'full_payment'
    | 'partial_payment'
    | 'credit'
    | 'credit_payment';
  PaymentObject?:
    | 'commodity'
    | 'excise'
    | 'job'
    | 'service'
    | 'gambling_bet'
    | 'gambling_prize'
    | 'lottery'
    | 'lottery_prize'
    | 'intellectual_activity'
    | 'payment'
    | 'agent_commission'
    | 'composite'
    | 'another';
}

export interface TelegramValues {
  telegramId: number;
}

export interface PaymentValues {
  lang: 'ru' | 'en';
  amount: number;
}

export interface IUser {
  _id: string;
  username: string;
  balance: number;
  refBalance: number;
  freePoints: number;
  type: string;
}

export interface UserAuth {
  token?: string;
  username: string;
  password: string;
}

export interface ProxyFormProps {
  host: String;
  port: String;
  comment?: String;
}

export interface ProxyFormState {
  todo: Boolean;
}

export interface IdMatchParams {
  id: string;
}

export type CodeValues = { code: number };

export interface FormDialogProps {
  dialogTitle?: string;
  dialogContent?: string;
  initialValues: object;
  validationSchema: any;
  fields: any[];
  handleSubmit: () => void;
  handleClose: () => void;
}

export interface AccountProps {
  _id: string;
  username: string;
  status: string;
  expired: boolean;
  expirationDate: Date;
  newbie: boolean;
  type: string;
  comment: string;
  proxy: string;
  data: {
    profile_pic_url: string;
    full_name: string;
  };
}

export interface IProxy {
  _id: string;
  host: string;
  comment: string;
  username: string;
  password: string;
  user: string;
}

export interface IAccountProps {
  data: AccountProps;
}

export interface ConfirmDialogProps {
  title: string;
  handleConfirm: () => void;
}

enum HeardFrom {
  SEARCH_ENGINE = 'Search Engine',
  FRIEND = 'Friend',
  OTHER = 'Other'
}
