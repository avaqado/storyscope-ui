import React from 'react';
import { FormikProps, Form, Field } from 'formik';
import { FormattedMessage, useIntl } from 'react-intl';
import { TelegramValues } from '../types';
import { Button, makeStyles, Theme } from '@material-ui/core';
import { TextField } from 'formik-material-ui';

const useStyles = makeStyles((theme: Theme) => ({
  form: {
    width: '100%',
    marginTop: theme.spacing(2)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

export const TelegramForm = (props: FormikProps<TelegramValues>) => {
  const { values, isSubmitting } = props;
  const classes = useStyles();
  const intl = useIntl();

  return (
    <Form className={classes.form}>
      <Field
        label={intl.formatMessage({ id: 'telegramId' })}
        component={TextField}
        id='telegramId'
        name='telegramId'
        variant='outlined'
        type='number'
        margin='normal'
        fullWidth
      />

      <Button
        type='submit'
        variant='contained'
        color='primary'
        size='large'
        disabled={isSubmitting}
        className={classes.submit}
      >
        <FormattedMessage id='save' />
      </Button>

      <div>{JSON.stringify(values, null, 4)}</div>
    </Form>
  );
};
