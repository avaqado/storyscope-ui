import React from 'react';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import EmailIcon from '@material-ui/icons/Email';
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';
import TelegramIcon from '@material-ui/icons/Telegram';
import { AppContainer } from '../app/AppContainer';
import { SubtitleLink } from '../components/SubtitleLink';
import { PAYMENTS, PROFILE } from '../api/routes';
import { useAuth } from '../context/auth';
import { FormattedNumber, FormattedMessage } from 'react-intl';
import {
  Card,
  List,
  ListItemIcon,
  ListItemText,
  ListItem,
  Link as MuiLink,
  Box
} from '@material-ui/core';
import { PaymentFeed } from '../payment/PaymentFeed';
import { Link } from 'react-router-dom';
import { Fab } from '../components/Fab';

export const ProfilePage = () => {
  return (
    <>
      <AppContainer
        title='profile'
        subtitle={<SubtitleLink to={PAYMENTS.ADD} title='addBalance' />}
      >
        <Profile />
        <Fab link={PAYMENTS.ADD}>
          <MonetizationOnIcon />
        </Fab>
      </AppContainer>

      <Box my={6} />

      <AppContainer title='paymentHistory'>
        <PaymentFeed />
      </AppContainer>
    </>
  );
};

const Profile = () => {
  const { user } = useAuth();

  const id = (
    <MuiLink component={Link} to={PROFILE.TELEGRAM}>
      {user.telegramId ? user.telegramId : <FormattedMessage id='add' />}
    </MuiLink>
  );

  const sum = (
    <FormattedNumber value={user.balance} style={`currency`} currency='RUB' />
  );

  return (
    <>
      <Card>
        <List>
          <ListItem>
            <ListItemIcon>
              <EmailIcon />
            </ListItemIcon>
            <ListItemText>
              <FormattedMessage id='email' values={{ email: user.username }} />
            </ListItemText>
          </ListItem>

          <ListItem>
            <ListItemIcon>
              <AccountBalanceWalletIcon />
            </ListItemIcon>
            <ListItemText>
              <FormattedMessage id='balance' values={{ sum }} />
            </ListItemText>
          </ListItem>

          <ListItem>
            <ListItemIcon>
              <TelegramIcon />
            </ListItemIcon>
            <ListItemText>
              <FormattedMessage id='telegram' values={{ id }} />
            </ListItemText>
          </ListItem>
        </List>
      </Card>
    </>
  );
};
