import React, { useState } from 'react';
import { AppContainer } from '../app/AppContainer';
import { Formik, FormikActions } from 'formik';
import { TelegramValues, UserValues } from '../types';
import { Redirect } from 'react-router';
import { handleFormError } from '../utils/handleResponse';
import { useIntl } from 'react-intl';
import { PROFILE } from '../api/routes';
import { TelegramForm } from './TelegramForm';
import { useAuth } from '../context/auth';
import { TelegramUpdateSchema } from '../validations/TelegramUpdate';
import { UserApi } from '../api';

export const TelegramAdd = () => (
  <AppContainer title='telegramAddTitle'>
    <AddForm />
  </AppContainer>
);

const AddForm = () => {
  const intl = useIntl();
  const { user, setUser } = useAuth();
  const [needRedirect, setRedirect] = useState(false);

  const updateAndRedirect = (values: TelegramValues) => {
    setUser((prev: UserValues) => ({ ...prev, ...values }));
    setRedirect(true);
  };

  const handleSubmit = (
    values: TelegramValues,
    actions: FormikActions<TelegramValues>
  ) => {
    UserApi.telegram(values)
      .then(() => updateAndRedirect(values))
      .catch((e: any) => handleFormError(e, actions))
      .finally(() => actions.setSubmitting(false));
  };

  if (needRedirect) return <Redirect to={PROFILE.INDEX} />;

  return (
    <Formik
      initialValues={{ telegramId: user.telegramId }}
      onSubmit={handleSubmit}
      validationSchema={TelegramUpdateSchema(intl)}
      render={props => <TelegramForm {...props} />}
    />
  );
};
