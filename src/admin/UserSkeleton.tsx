import React from 'react';
import { Skeleton } from '@material-ui/lab';
import {
  Card,
  CardHeader,
  Divider,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  CardActions,
  Theme,
  makeStyles,
  createStyles
} from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    card: {
      marginBottom: theme.spacing(3)
    }
  })
);

export const UserSkeleton: React.FC = () => (
  <Card className={useStyles().card}>
    <CardHeader
      avatar={<Skeleton variant='circle' height={50} width={50} />}
      title={<Skeleton height={15} width='10%' />}
      subheader={<Skeleton height={10} width='8%' />}
    />

    <Divider variant='middle' light />

    <List aria-label='data'>
      <ListItem>
        <ListItemIcon>
          <Skeleton variant='circle' height={25} width={25} />
        </ListItemIcon>
        <ListItemText
          disableTypography
          primary={<Skeleton height={15} width='8%' />}
          secondary={<Skeleton height={10} width='10%' />}
        />
      </ListItem>
      <ListItem>
        <ListItemIcon>
          <Skeleton variant='circle' height={25} width={25} />
        </ListItemIcon>
        <ListItemText
          disableTypography
          primary={<Skeleton height={15} width='12%' />}
          secondary={<Skeleton height={10} width='10%' />}
        />
      </ListItem>
    </List>

    <Divider variant='middle' light />

    <CardActions>
      <Skeleton height={20} width='6%' />
      <Skeleton height={20} width='8%' />
    </CardActions>
  </Card>
);
