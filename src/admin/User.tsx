import React from 'react';
import { makeStyles } from '@material-ui/styles';
import {
  Theme,
  createStyles,
  Card,
  List,
  ListItem,
  ListItemIcon,
  ListItemText
} from '@material-ui/core';
import { UserProps } from 'types';
import { FormattedMessage, FormattedNumber } from 'react-intl';

import EmailIcon from '@material-ui/icons/Email';
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    card: {
      marginBottom: theme.spacing(3)
    }
  })
);

export const User: React.FC<UserProps> = ({
  username,
  balance,
  refBalance
}) => {
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <List aria-label='data'>
        <ListItem>
          <ListItemIcon>
            <EmailIcon />
          </ListItemIcon>
          <ListItemText
            primary={username}
            secondary={<FormattedMessage id='emailAddress' />}
          />
        </ListItem>

        <ListItem>
          <ListItemIcon>
            <AccountBalanceWalletIcon />
          </ListItemIcon>
          <ListItemText
            primary={
              <FormattedNumber
                style={`currency`}
                currency='RUB'
                value={balance}
              />
            }
            secondary={<FormattedMessage id='balanceTitle' />}
          />
        </ListItem>

        <ListItem>
          <ListItemIcon>
            <AccountBalanceWalletIcon />
          </ListItemIcon>
          <ListItemText
            primary={
              <FormattedNumber
                style={`currency`}
                currency='RUB'
                value={refBalance}
              />
            }
            secondary={<FormattedMessage id='refBalanceTitle' />}
          />
        </ListItem>
      </List>
    </Card>
  );
};
