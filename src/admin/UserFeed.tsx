import React, { useState } from 'react';
import { AppContainer } from 'app/AppContainer';
import { UserSkeleton } from './UserSkeleton';
import { USERS } from 'api/routes';
import { useGet } from 'hooks';
import { UserProps } from 'types';
import { User } from './User';
import { Typography, Box, TextField } from '@material-ui/core';
import { FormattedMessage } from 'react-intl';

export const UserFeed: React.FC = () => {
  const [params, setParams] = useState({});
  const { data, error, isLoading } = useGet(USERS.ALL, params);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const username = event.target.value;

    if (username) setParams({ username });
    else setParams({});
  };

  if (error) return <>{error}</>;
  return (
    <AppContainer
      title='userList'
      subtitle={
        !isLoading && (
          <Box>
            <Typography>
              <FormattedMessage id='userCount' values={{ count: data.count }} />
            </Typography>
          </Box>
        )
      }
    >
      <TextField
        // className={classes.textField}
        // defaultValue='Bare'
        onChange={handleChange}
        margin='normal'
        variant='outlined'
        fullWidth
      />
      {isLoading ? (
        <>
          <UserSkeleton />
          <UserSkeleton />
          <UserSkeleton />
        </>
      ) : (
        data.users.map((user: UserProps) => <User key={user._id} {...user} />)
      )}
    </AppContainer>
  );
};
