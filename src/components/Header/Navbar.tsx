import React from 'react';
import { makeStyles, createStyles } from '@material-ui/styles';
import { Menu as MenuIcon } from '@material-ui/icons';
import { DrawerContent } from './DrawerContent';
import { LanguageMenu } from './LanguageMenu';
import { ProfileMenu } from './ProfileMenu';
import {
  IconButton,
  Typography,
  AppBar,
  Toolbar,
  Hidden,
  Drawer,
  useTheme,
  Theme
} from '@material-ui/core';

interface HeaderProps {
  open: boolean;
  mobileOpen: boolean;
  desktopOnClick: () => void;
  mobileOnClick: () => void;
}

const drawerWidth = 240;
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    grow: {
      flexGrow: 1
    },
    root: {
      display: 'flex'
    },
    container: {
      [theme.breakpoints.down('xs')]: {
        padding: theme.spacing(0)
      }
    },
    title: {
      marginBottom: theme.spacing(3)
    },
    drawer: {
      [theme.breakpoints.up('sm')]: {
        width: drawerWidth,
        flexShrink: 0
      }
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      })
    },
    desktopMenuButton: {
      marginLeft: theme.spacing(-2),
      marginRight: theme.spacing(2),
      [theme.breakpoints.down('xs')]: {
        display: 'none'
      }
    },
    mobileMenuButton: {
      marginRight: theme.spacing(0),
      [theme.breakpoints.up('sm')]: {
        display: 'none'
      }
    },
    drawerIcon: {
      marginLeft: theme.spacing(1)
    },
    drawerItem: {
      marginLeft: theme.spacing(0),
      paddingLeft: theme.spacing(0)
    },
    drawerPaper: {
      width: drawerWidth
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      }),
      [theme.breakpoints.up('sm')]: {
        marginLeft: -drawerWidth
      }
    },
    contentShift: {
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen
      }),
      marginLeft: 0
    },
    divider: {
      marginTop: 10,
      marginBottom: 10
    }
  })
);

export const Header: React.FC<HeaderProps> = ({
  open,
  mobileOpen,
  desktopOnClick,
  mobileOnClick
}) => {
  const classes = useStyles();
  const theme = useTheme();

  return (
    <>
      <AppBar position='fixed' color='primary' className={classes.appBar}>
        <Toolbar>
          {/* Mobile menu icon */}
          <IconButton
            color='inherit'
            aria-label='open drawer'
            edge='start'
            onClick={mobileOnClick}
            className={classes.mobileMenuButton}
          >
            <MenuIcon />
          </IconButton>

          {/* Desktop menu icon */}
          <IconButton
            color='inherit'
            aria-label='open drawer'
            onClick={desktopOnClick}
            edge='start'
            className={classes.desktopMenuButton}
          >
            <MenuIcon />
          </IconButton>

          <Typography variant='h6' noWrap>
            Storyscope
          </Typography>

          <div className={classes.grow} />

          <LanguageMenu />
          <ProfileMenu />
        </Toolbar>
      </AppBar>

      <nav className={classes.drawer} aria-label='links'>
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation='css'>
          <Drawer
            variant='temporary'
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={mobileOnClick}
            classes={{
              paper: classes.drawerPaper
            }}
            ModalProps={{
              keepMounted: true // Better open performance on mobile.
            }}
          >
            <DrawerContent handleClick={mobileOnClick} />
          </Drawer>
        </Hidden>

        <Hidden xsDown implementation='css'>
          <Drawer
            className={classes.drawer}
            variant='persistent'
            anchor='left'
            open={open}
            classes={{
              paper: classes.drawerPaper
            }}
          >
            <DrawerContent />
          </Drawer>
        </Hidden>
      </nav>
    </>
  );
};
