import React from 'react';
import { useAuth } from '../../context/auth';
import { MenuItem, Menu, Divider, IconButton } from '@material-ui/core';
import { FormattedNumber, FormattedMessage } from 'react-intl';
import { PAYMENTS, PROFILE } from '../../api/routes';
import { Link } from 'react-router-dom';
import FaceIcon from '@material-ui/icons/Face';

interface MenuItemsProps {
  anchor: HTMLElement | null;
  handleClose: () => void;
}

// const useStyles = makeStyles({
//   orangeAvatar: {
//     margin: 10,
//     color: '#fff',
//     backgroundColor: deepOrange[500],
//   }
// });

export const ProfileMenu = () => {
  const [anchor, setAnchor] = React.useState<null | HTMLElement>(null);
  // const { user } = useAuth();
  // const classes = useStyles();
  // const letter = user.username[0].toUpperCase();

  const handleOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchor(event.currentTarget);
  };

  const handleClose = () => {
    setAnchor(null);
  };

  return (
    <>
      {/* <Avatar className={classes.orangeAvatar} onClick={handleOpen}>{letter}</Avatar> */}
      <IconButton
        edge='end'
        aria-label='profile-menu'
        aria-controls='profile-menu'
        aria-haspopup='true'
        color='inherit'
        onClick={handleOpen}
      >
        <FaceIcon />
      </IconButton>

      <MenuItems {...{ anchor, handleClose }} />
    </>
  );
};

const MenuItems: React.FC<MenuItemsProps> = ({ anchor, handleClose }) => {
  const { signout, user } = useAuth();

  const sum = (
    <FormattedNumber style={`currency`} value={user.balance} currency='RUB' />
  );

  return (
    <Menu
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={Boolean(anchor)}
      onClose={handleClose}
      anchorEl={anchor}
      id='profile-menu'
      keepMounted
    >
      <MenuItem onClick={handleClose} component={Link} to={PAYMENTS.ADD}>
        <FormattedMessage id='balance' values={{ sum }} />
      </MenuItem>

      <Divider light />

      <MenuItem onClick={handleClose} component={Link} to={PROFILE.INDEX}>
        <FormattedMessage id='profile' />
      </MenuItem>

      <MenuItem onClick={handleClose} component={Link} to={PROFILE.REFS}>
        <FormattedMessage id='affiliateProgram' />
      </MenuItem>

      <Divider light />

      <MenuItem onClick={signout}>
        <FormattedMessage id='signOut' />
      </MenuItem>
    </Menu>
  );
};
