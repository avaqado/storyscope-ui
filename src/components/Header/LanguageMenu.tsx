import React from 'react';
import TranslateIcon from '@material-ui/icons/Translate';
// import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {
  Typography,
  Menu,
  MenuItem,
  makeStyles,
  Theme,
  createStyles,
  Button
} from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import { useLocalStorage } from '../../hooks/useLocalStorage';

interface MenuItemsProps {
  anchor: HTMLElement | null;
  handleClose: () => void;
  setLocale: (lang: string, reload: boolean) => void;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    langIcon: {
      marginRight: theme.spacing(1)
    }
  })
);

export const LanguageMenu = () => {
  const [anchor, setAnchor] = React.useState<null | HTMLElement>(null);
  const [locale, setLocale] = useLocalStorage('locale', 'en-US');
  const classes = useStyles();

  const handleOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchor(event.currentTarget);
  };

  const handleClose = () => {
    setAnchor(null);
  };

  return (
    <>
      <Button color='inherit' onClick={handleOpen}>
        <Typography className={classes.langIcon}>
          <FormattedMessage id={locale} />
        </Typography>
        <TranslateIcon className={classes.langIcon} />
        {/* <ExpandMoreIcon /> */}
      </Button>

      <MenuItems {...{ anchor, handleClose, setLocale }} />
    </>
  );
};

const MenuItems: React.FC<MenuItemsProps> = ({
  anchor,
  handleClose,
  setLocale
}) => {
  const switchRu = () => setLocale('ru-RU', true);
  const switchEn = () => setLocale('en-US', true);

  return (
    <Menu
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={Boolean(anchor)}
      onClose={handleClose}
      anchorEl={anchor}
      id='profile-menu'
      keepMounted
    >
      <MenuItem dense onClick={switchEn}>
        <FormattedMessage id='en' />
      </MenuItem>
      <MenuItem dense onClick={switchRu}>
        <FormattedMessage id='ru' />
      </MenuItem>
    </Menu>
  );
};
