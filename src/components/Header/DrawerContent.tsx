import React, { useState } from 'react';
import { makeStyles, createStyles } from '@material-ui/styles';
import { Link } from 'react-router-dom';
import { ACCOUNTS, TASKS, USERS } from 'api/routes';
import { FormattedMessage } from 'react-intl';
import {
  Divider,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Theme,
  Typography,
  Toolbar,
  Collapse
} from '@material-ui/core';
import {
  Class,
  Group,
  SubdirectoryArrowRight,
  Whatshot,
  ExpandLess,
  ExpandMore
} from '@material-ui/icons';
import { useAuth } from 'context/auth';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    divider: {
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(1)
    },
    nested: {
      // paddingLeft: theme.spacing(4)
    },
    toolbar: theme.mixins.toolbar
  })
);

interface DrawerProps {
  handleClick?: () => void;
}

export const DrawerContent: React.FC<DrawerProps> = ({ handleClick }) => {
  const classes = useStyles();
  const { user } = useAuth();

  const [adminOpen, setAdminOpen] = useState(false);
  const handleOpenPanel = () => setAdminOpen(!adminOpen);

  return (
    <>
      <Toolbar>
        <Typography variant='button'>
          <FormattedMessage id='menu' />
        </Typography>
      </Toolbar>
      <Divider />
      <List>
        <ListItem onClick={handleClick} component={Link} to={TASKS.ALL} button>
          <ListItemIcon>
            <Class />
          </ListItemIcon>
          <ListItemText primary={<FormattedMessage id='tasks' />} />
        </ListItem>

        <ListItem
          onClick={handleClick}
          component={Link}
          to={ACCOUNTS.ALL}
          button
        >
          <ListItemIcon>
            <Group />
          </ListItemIcon>
          <ListItemText primary={<FormattedMessage id='accounts' />} />
        </ListItem>

        <ListItem onClick={handleClick} component={Link} to='/proxies' button>
          <ListItemIcon>
            <SubdirectoryArrowRight />
          </ListItemIcon>
          <ListItemText primary={<FormattedMessage id='proxies' />} />
        </ListItem>

        <Divider className={classes.divider} />

        {user.type === 'admin' && (
          <>
            <ListItem onClick={handleOpenPanel} button>
              <ListItemIcon>
                <Whatshot />
              </ListItemIcon>
              <ListItemText primary={<FormattedMessage id='adminPanel' />} />
              {adminOpen ? <ExpandLess /> : <ExpandMore />}
            </ListItem>

            <Collapse in={adminOpen} timeout='auto' unmountOnExit>
              <List component='div' disablePadding>
                <ListItem
                  className={classes.nested}
                  onClick={handleClick}
                  component={Link}
                  to={USERS.ALL}
                  dense
                  button
                >
                  <ListItemText primary={<FormattedMessage id='userList' />} />
                </ListItem>

                <ListItem
                  className={classes.nested}
                  onClick={handleClick}
                  component={Link}
                  to={TASKS.ACTIVE}
                  dense
                  button
                >
                  <ListItemText
                    primary={<FormattedMessage id='activeTasks' />}
                  />
                </ListItem>
              </List>
            </Collapse>
          </>
        )}
      </List>
    </>
  );
};
