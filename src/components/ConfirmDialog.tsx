import React from 'react';
import { Dialog, DialogTitle, DialogActions, Button } from '@material-ui/core';
import { FormattedMessage } from 'react-intl';

interface RemoveDialogProps {
  title: string;
  handleConfirm: any;
  handleClose: any;
}

export const ConfirmDialog: React.FC<RemoveDialogProps> = ({
  title,
  handleConfirm,
  handleClose
}) => (
  <Dialog open onClose={handleClose} aria-labelledby='confirm-dialog-title'>
    <DialogTitle id='confirm-dialog-title'>
      <FormattedMessage id={title} />
    </DialogTitle>
    <DialogActions>
      <Button onClick={handleClose} color='primary'>
        <FormattedMessage id='cancel' />
      </Button>
      <Button onClick={handleConfirm} color='primary' autoFocus>
        <FormattedMessage id='yes' />
      </Button>
    </DialogActions>
  </Dialog>
);
