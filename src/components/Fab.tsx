import React from 'react';
import { Fab as MuiFab, Theme, createStyles } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { Link } from 'react-router-dom';

type FabProps = {
  link: string;
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    fab: {
      position: 'fixed',
      bottom: theme.spacing(2),
      right: theme.spacing(2)
    }
  })
);

export const Fab: React.FC<FabProps> = ({ link, children }) => {
  const classes = useStyles();

  return (
    <Link to={link}>
      <MuiFab color='primary' aria-label='add' className={classes.fab}>
        {children}
      </MuiFab>
    </Link>
  );
};
