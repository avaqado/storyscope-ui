import React from 'react';
import { CircularProgress, Button, makeStyles, Theme } from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import { ButtonProps } from '@material-ui/core/Button';

interface LoadingButtonProps extends ButtonProps {
  title: string;
  isLoading: boolean;
}

const useStyles = makeStyles((theme: Theme) => ({
  submit: {
    margin: theme.spacing(2, 0, 2)
  },
  progress: {
    color: '#ffffff'
  }
}));

export const LoadingButton: React.FC<LoadingButtonProps> = ({
  title,
  isLoading,
  ...other
}) => {
  const classes = useStyles();
  const startIcon = isLoading && (
    <CircularProgress className={classes.progress} size={20} />
  );

  return (
    <Button
      className={classes.submit}
      variant='contained'
      color='primary'
      type='submit'
      startIcon={startIcon}
      disabled={isLoading}
      {...other}
    >
      <FormattedMessage id={title} />
    </Button>
  );
};
