import React from 'react';
import { Typography, Button, Grid } from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import Link from '@material-ui/core/Link';

interface ErrorState {
  hasError: boolean;
}

export class ErrorBoundary extends React.Component<any, ErrorState> {
  constructor(props: any) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError() {
    return { hasError: true };
  }

  componentDidCatch() {
    // Можно также сохранить информацию об ошибке в соответствующую службу журнала ошибок
  }

  render() {
    if (this.state.hasError) {
      // Можно отрендерить запасной UI произвольного вида
      return (
        <Grid container direction='column' alignItems='center'>
          <Typography gutterBottom variant='h6'>
            <FormattedMessage id='wentWorng' />
          </Typography>

          <Link href=''>
            <Button variant='outlined'>
              <FormattedMessage id='home' />
            </Button>
          </Link>
        </Grid>
      );
    }

    return this.props.children;
  }
}
