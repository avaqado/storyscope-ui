import React, { useState } from 'react';
import red from '@material-ui/core/colors/red';
import FaceIcon from '@material-ui/icons/Face';
import HelpIcon from '@material-ui/icons/Help';
import { makeStyles } from '@material-ui/styles';
import {
  Link,
  Avatar as MuiAvatar,
  Theme,
  createStyles
} from '@material-ui/core';

interface AvatarProps {
  src: string;
  username: string;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    avatar: {
      backgroundColor: red[500]
    },
    bigAvatar: {
      width: 50,
      height: 50
    }
  })
);

export const Avatar: React.FC<AvatarProps> = ({ src, username }) => {
  const classes = useStyles();
  const [hasError, setError] = useState<boolean>(false);

  return (
    <Link href={`https://instagram.com/${username}`}>
      {hasError ? (
        <MuiAvatar aria-label='avatar' className={classes.bigAvatar}>
          <FaceIcon />
        </MuiAvatar>
      ) : src ? (
        <MuiAvatar
          aria-label='avatar'
          src={src}
          onError={() => setError(true)}
          className={classes.bigAvatar}
        />
      ) : (
        <MuiAvatar aria-label='avatar' className={classes.bigAvatar}>
          <HelpIcon />
        </MuiAvatar>
      )}
    </Link>
  );
};
