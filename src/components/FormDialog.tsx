import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Formik, Form, Field } from 'formik';
import { DialogContentText, CircularProgress, Theme } from '@material-ui/core';
import { useIntl } from 'react-intl';
import { FormDialogProps } from '../types';
import { TextField } from 'formik-material-ui';
import { makeStyles, createStyles } from '@material-ui/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    progress: {
      marginRight: theme.spacing(2),
      marginBottom: theme.spacing(1)
    }
  })
);

export const FormDialog: React.FC<FormDialogProps> = ({
  initialValues,
  validationSchema,
  handleSubmit,
  ...formProps
}) => (
  <Formik
    initialValues={initialValues}
    onSubmit={handleSubmit}
    validationSchema={validationSchema}
    render={props => <InnerForm {...props} {...formProps} />}
  />
);

const InnerForm: React.FC<any> = ({
  handleClose,
  fields,
  dialogTitle,
  dialogContent,
  submitForm,
  isSubmitting
}) => {
  const classes = useStyles();
  const intl = useIntl();

  return (
    <Dialog
      open
      // fullWidth
      onClose={handleClose}
      aria-labelledby='form-dialog-title'
    >
      {dialogTitle && (
        <DialogTitle id='form-dialog-title'>
          {intl.formatMessage({ id: dialogTitle })}
        </DialogTitle>
      )}
      <DialogContent>
        {dialogContent && (
          <DialogContentText>
            {intl.formatMessage({ id: dialogContent })}
          </DialogContentText>
        )}
        <Form>
          {fields.map((field: any) => (
            <Field
              {...field}
              key={field.id}
              label={intl.formatMessage({ id: field.label })}
              component={TextField}
              variant='outlined'
              type='number'
              fullWidth
            />
          ))}
        </Form>
      </DialogContent>

      <DialogActions>
        {isSubmitting ? (
          <CircularProgress className={classes.progress} size={25} />
        ) : (
          <>
            <Button onClick={handleClose} color='primary'>
              {intl.formatMessage({ id: 'cancel' })}
            </Button>
            <Button onClick={submitForm} color='primary'>
              {intl.formatMessage({ id: 'ok' })}
            </Button>
          </>
        )}
      </DialogActions>
    </Dialog>
  );
};
