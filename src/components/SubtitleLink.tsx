import React from 'react';
import { makeStyles, createStyles } from '@material-ui/styles';
import { Theme, Link as MuiLink } from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';

interface SubtitleLinkProps {
  title: string;
  to: string;
  variant?: 'subtitle1' | 'subtitle2';
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    add: {
      marginBottom: theme.spacing(2)
    }
  })
);

export const SubtitleLink: React.FC<SubtitleLinkProps> = ({
  to,
  title,
  variant = 'subtitle1'
}) => {
  const classes = useStyles();

  return (
    <div className={classes.add}>
      <MuiLink component={Link} {...{ to, variant }}>
        <FormattedMessage id={title} />
      </MuiLink>
    </div>
  );
};
