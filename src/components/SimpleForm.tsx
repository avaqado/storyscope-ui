import React from 'react';
import { Formik, Field, Form, FormikErrors } from 'formik';
import { string, object } from 'yup';

const initialValues = {
  firstName: '',
  pet: ''
};

interface FormValues {
  firstName: string;
  pet: string;
}

export const SimpleForm: React.FC = () => (
  <>
    <h1>Working with Formik</h1>
    <Formik
      initialValues={initialValues}
      onSubmit={values => console.log(values)}
      validationSchema={object().shape({
        firstName: string().required('Entering your first name is required.')
      })}
      render={({ handleChange, errors, touched }) => (
        <Form>
          <label htmlFor='firstName'>
            <div>First Name</div>
            <Field type='text' name='firstName' />
            {touched.firstName && errors.firstName ? (
              <div>{errors.firstName}</div>
            ) : null}
          </label>
          <label htmlFor='pet'>
            <div>Pet</div>
            <Field name='pet' onChange={handleChange} component='select'>
              <option>Dog</option>
              <option>Cat</option>
              <option>Other</option>
            </Field>
          </label>
          <button type='submit'>Submit</button>
        </Form>
      )}
    />
  </>
);
