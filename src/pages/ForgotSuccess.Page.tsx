import React from 'react';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { FormattedMessage } from 'react-intl';
import { grey } from '@material-ui/core/colors';
import {
  Container,
  CssBaseline,
  Avatar,
  Typography,
  makeStyles,
  Theme,
  Button
} from '@material-ui/core';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme: Theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: grey[900]
  }
}));

export const ForgotSuccessPage = () => {
  const classes = useStyles();

  return (
    <Container component='main' maxWidth='lg'>
      <CssBaseline />

      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>

        <Typography component='h1' variant='h5' gutterBottom>
          <FormattedMessage id='mailSent' />
        </Typography>

        <Button component={Link} to='/' size='large' variant='outlined'>
          <FormattedMessage id='home' />
        </Button>
      </div>
    </Container>
  );
};
