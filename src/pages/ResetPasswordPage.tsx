import axios from 'axios';
import React, { useState, useEffect } from 'react';
import Avatar from '@material-ui/core/Avatar';
import MuiLink from '@material-ui/core/Link';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles, createMuiTheme } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { Formik, Field, Form, FormikProps } from 'formik';
import { useAuth } from '../context/auth';
import { TextField } from 'formik-material-ui';
import { ThemeProvider } from '@material-ui/styles';
import { grey } from '@material-ui/core/colors';
import { FormattedMessage, useIntl } from 'react-intl';
import { LoadingButton } from '../components/LoadingButton';
import { Link, RouteComponentProps, Redirect } from 'react-router-dom';
import { sessionApi } from '../api';
import { Loading } from '../components/Loading';
import { ResetPasswordValues } from '../types';
import { PasswordUpdateSchema } from '../validations/PasswordUpdate';

const theme = createMuiTheme({
  palette: {
    primary: { main: grey[900] },
    secondary: { main: '#11cb5f' }
  }
});

const useStyles = makeStyles(theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white
    }
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: grey[900]
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  }
}));

interface TokenMatchParams {
  token: string;
}

type ResetPasswordProps = RouteComponentProps<TokenMatchParams>;

export const ResetPasswordPage: React.FC<ResetPasswordProps> = ({ match }) => {
  const token = match.params.token;
  const classes = useStyles();
  const intl = useIntl();
  const { updatePassword } = useAuth();
  const [isLoading, setLoading] = useState(true);
  const [needRedirect, setRedirect] = useState(false);

  const initialValues: ResetPasswordValues = {
    password: '',
    confirmPassword: '',
    token
  };

  useEffect(() => {
    let mounted = true;
    let source = axios.CancelToken.source();

    sessionApi
      .checkResetToken(token)
      .then(() => mounted && setLoading(false))
      .catch(() => mounted && setRedirect(true))
      .finally(() => setLoading(false));

    return () => {
      mounted = false;
      source.cancel('Cancelling in cleanup');
    };
  }, [token]);

  if (needRedirect) return <Redirect to='/' />;
  if (isLoading) return <Loading />;

  return (
    <ThemeProvider theme={theme}>
      <Container component='main' maxWidth='xs'>
        <CssBaseline />

        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>

          <Typography component='h1' variant='h5'>
            <FormattedMessage id='newPassword' />
          </Typography>

          <Formik
            onSubmit={updatePassword}
            initialValues={initialValues}
            validationSchema={PasswordUpdateSchema(intl)}
            render={props => <InnerForm {...props} />}
          />
        </div>
      </Container>
    </ThemeProvider>
  );
};

function InnerForm(props: FormikProps<ResetPasswordValues>) {
  const { status, isSubmitting } = props;
  const classes = useStyles();

  return (
    <Form className={classes.form}>
      {status && (
        <Typography color='error'>
          <FormattedMessage id={status} />
        </Typography>
      )}

      <Field
        label={<FormattedMessage id='password' />}
        component={TextField}
        autoComplete='none'
        name='password'
        variant='outlined'
        margin='normal'
        type='password'
        id='password'
        required
        fullWidth
      />

      <Field
        label={<FormattedMessage id='confirmPassword' />}
        component={TextField}
        autoComplete='none'
        name='confirmPassword'
        variant='outlined'
        margin='normal'
        type='password'
        id='confirmPassword'
        required
        fullWidth
      />

      <LoadingButton
        fullWidth
        size='large'
        title='save'
        isLoading={isSubmitting}
      />

      <Grid container>
        <Grid item xs>
          <MuiLink component={Link} to='/' variant='body2'>
            <FormattedMessage id='home' />
          </MuiLink>
        </Grid>
        <Grid item>
          <MuiLink component={Link} to='/auth' variant='body2'>
            <FormattedMessage id='login' />
          </MuiLink>
        </Grid>
      </Grid>
    </Form>
  );
}
