import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import MuiLink from '@material-ui/core/Link';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles, createMuiTheme } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { Formik, Field, Form, FormikProps } from 'formik';
import { SigninSchema } from '../validations';
import { UserAuth } from '../types';
import { useAuth } from '../context/auth';
import { TextField } from 'formik-material-ui';
import { ThemeProvider } from '@material-ui/styles';
import { grey } from '@material-ui/core/colors';
import { FormattedMessage, useIntl } from 'react-intl';
import { LoadingButton } from '../components/LoadingButton';
import { Link } from 'react-router-dom';

const theme = createMuiTheme({
  palette: {
    primary: { main: grey[900] },
    secondary: { main: '#11cb5f' }
  }
});

const useStyles = makeStyles(theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white
    }
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: grey[900]
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  }
}));

export const LoginPage = () => {
  const initialValues: UserAuth = { username: '', password: '' };
  const classes = useStyles();
  const intl = useIntl();
  const { login } = useAuth();

  return (
    <ThemeProvider theme={theme}>
      <Container component='main' maxWidth='xs'>
        <CssBaseline />

        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>

          <Typography component='h1' variant='h5'>
            <FormattedMessage id='login' />
          </Typography>

          <Formik
            onSubmit={login}
            initialValues={initialValues}
            validationSchema={SigninSchema(intl)}
            render={props => <InnerForm {...props} />}
          />
        </div>
      </Container>
    </ThemeProvider>
  );
};

function InnerForm(props: FormikProps<UserAuth>) {
  const { status, isSubmitting } = props;
  const classes = useStyles();

  return (
    <Form className={classes.form}>
      {status && (
        <Typography color='error'>
          <FormattedMessage id={status} />
        </Typography>
      )}

      <Field
        label='Email'
        component={TextField}
        autoComplete='email'
        id='email'
        name='username'
        variant='outlined'
        margin='normal'
        required
        fullWidth
      />

      <Field
        label={<FormattedMessage id='password' />}
        component={TextField}
        autoComplete='current-password'
        name='password'
        variant='outlined'
        margin='normal'
        type='password'
        id='password'
        required
        fullWidth
      />

      <LoadingButton
        fullWidth
        size='large'
        title='login'
        isLoading={isSubmitting}
      />

      <Grid container>
        <Grid item xs>
          <MuiLink component={Link} to='auth/forgot' variant='body2'>
            <FormattedMessage id='forgotPassword' />
          </MuiLink>
        </Grid>
        <Grid item>
          <MuiLink component={Link} to='auth/register' variant='body2'>
            <FormattedMessage id='hasNoAccount' />
          </MuiLink>
        </Grid>
      </Grid>
    </Form>
  );
}
