import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import CssBaseline from '@material-ui/core/CssBaseline';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles, createMuiTheme } from '@material-ui/core/styles';
import { Formik, Field, Form, FormikProps, FormikActions } from 'formik';
import { ResetProps } from '../types';
import { TextField } from 'formik-material-ui';
import { ThemeProvider } from '@material-ui/styles';
import { grey } from '@material-ui/core/colors';
import { FormattedMessage, useIntl } from 'react-intl';
import { LoadingButton } from '../components/LoadingButton';
import { Redirect } from 'react-router-dom';
import { PasswordResetSchema } from '../validations/PasswordReset';
import { SESSION } from '../api/routes';
import { Container } from '@material-ui/core';
import { sessionApi } from '../api';

const theme = createMuiTheme({
  palette: {
    primary: { main: grey[900] },
    secondary: { main: '#11cb5f' }
  }
});

const useStyles = makeStyles(theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white
    }
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: grey[900]
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  }
}));

export const ForgotPage = () => {
  const initialValues: ResetProps = { username: '' };
  const classes = useStyles();
  const intl = useIntl();
  const [needRedirect, setRedirect] = useState(false);

  const handleSubmit = (
    values: ResetProps,
    actions: FormikActions<ResetProps>
  ) => {
    sessionApi
      .reset(values)
      .then(() => setRedirect(true))
      .catch(() => actions.setStatus('hasError'))
      .finally(() => actions.setSubmitting(false));
  };

  if (needRedirect) return <Redirect to={SESSION.FORGOT_SUCCESS} />;

  return (
    <ThemeProvider theme={theme}>
      <Container component='main' maxWidth='xs'>
        <CssBaseline />

        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>

          <Typography component='h1' variant='h5'>
            <FormattedMessage id='resetPassword' />
          </Typography>

          <Formik
            onSubmit={handleSubmit}
            initialValues={initialValues}
            validationSchema={PasswordResetSchema(intl)}
            render={props => <InnerForm {...props} />}
          />
        </div>
      </Container>
    </ThemeProvider>
  );
};

function InnerForm(props: FormikProps<ResetProps>) {
  const classes = useStyles();
  const { status, isSubmitting } = props;

  return (
    <Form className={classes.form}>
      {status && (
        <Typography color='error'>
          <FormattedMessage id={status} />
        </Typography>
      )}

      <Field
        label='Email'
        component={TextField}
        autoComplete='email'
        id='email'
        name='username'
        variant='outlined'
        margin='normal'
        required
        fullWidth
      />

      <LoadingButton
        fullWidth
        size='large'
        title='reset'
        isLoading={isSubmitting}
      />
    </Form>
  );
}
