import React from 'react';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import { Nav } from './common/CustomNav';
import { Header } from './common/Header';
import { Prices } from './common/Prices';
import { Footer } from './common/Footer';
import { Faq } from './common/Faq';
import { About } from './common/About';

const theme = createMuiTheme({
  typography: {
    fontFamily: 'Montserrat, Roboto, sans-serif'
  },
  palette: {
    type: 'dark',
    background: { default: '#000000' },
    contrastThreshold: 3,
    tonalOffset: 0.2,
    primary: {
      main: '#E5E5E5'
    },
    secondary: {
      main: '#14213D'
    },
    error: {
      main: '#FCA311'
    }
  }
});

export const LandingPage: React.FC = props => {
  return (
    <MuiThemeProvider theme={theme}>
      <CssBaseline />
      <Container maxWidth='lg'>
        <Nav />
        <Header />
        <About />
        <Prices />
        <Faq />
        <Footer />
      </Container>
    </MuiThemeProvider>
  );
};
