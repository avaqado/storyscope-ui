import React from 'react';
import { Grid, Typography, makeStyles, Theme, Box } from '@material-ui/core';
import { FormattedMessage, FormattedHTMLMessage } from 'react-intl';

interface SectionProps {
  title: string;
  subtitle: string;
}

const useStyles = makeStyles((theme: Theme) => ({
  mt8: {
    marginTop: theme.spacing(8)
  },
  textAlignCenter: {
    textAlign: 'center'
  }
}));

export const Section: React.FC<SectionProps> = ({
  title,
  subtitle,
  children
}) => {
  const classes = useStyles();

  return (
    <Box my={15}>
      <Grid
        container
        className={classes.mt8}
        direction='column'
        alignItems='center'
      >
        <Typography gutterBottom variant='h4'>
          <FormattedMessage id={title} />
        </Typography>

        <Grid item md={8}>
          <Box mb={4}>
            <Typography className={classes.textAlignCenter} variant='subtitle1'>
              <FormattedHTMLMessage id={subtitle} />
            </Typography>
          </Box>
        </Grid>
      </Grid>

      {children}
    </Box>
  );
};
