import React from 'react';
import PanoramaFishEyeIcon from '@material-ui/icons/PanoramaFishEye';
import { makeStyles } from '@material-ui/styles';
import { FormattedMessage } from 'react-intl';
import { useLocalStorage } from '../../hooks';
import {
  Theme,
  Grid,
  Typography,
  Button,
  ButtonGroup
} from '@material-ui/core';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme: Theme) => ({
  header: {
    marginTop: theme.spacing(4)
  },
  langButton: {
    marginRight: theme.spacing(1)
  },
  navGrid: {
    marginTop: theme.spacing(4),
    [theme.breakpoints.down('sm')]: {
      justifyContent: 'space-between'
    },
    [theme.breakpoints.up('md')]: {
      justifyContent: 'flex-end'
    }
  },
  logoOrder: {
    order: 2,
    [theme.breakpoints.down('sm')]: {
      marginTop: theme.spacing(3)
    }
  },
  langOrder: {
    [theme.breakpoints.down('sm')]: {
      order: 1,
      justifyContent: 'space-between'
    },
    [theme.breakpoints.up('md')]: {
      order: 3,
      justifyContent: 'flex-end'
    }
  },
  mt8: {
    marginTop: theme.spacing(8)
  },
  mt6: {
    marginTop: theme.spacing(6)
  },
  textAlignCenter: {
    textAlign: 'center'
  }
}));

export const Nav: React.FC = () => {
  const setLocale = useLocalStorage('locale', 'en-US')[1];
  const classes = useStyles();

  const switchEn = () => setLocale('en-US', true);
  const switchRU = () => setLocale('ru-RU', true);

  return (
    <Grid
      container
      direction='row'
      alignItems='center'
      className={classes.navGrid}
    >
      <Grid item md={3} />

      <Grid
        item
        md={6}
        xs={12}
        container
        className={classes.logoOrder}
        direction='column'
        justify='center'
        alignItems='center'
      >
        <PanoramaFishEyeIcon />
        <Typography variant='h5'>
          <FormattedMessage id='site' />
        </Typography>
      </Grid>

      <Grid
        item
        md={3}
        xs={12}
        container
        className={classes.langOrder}
        alignItems='center'
      >
        <ButtonGroup className={classes.langButton}>
          <Button onClick={switchRU} variant='text'>
            <FormattedMessage id='ru-RU' />
          </Button>

          <Button onClick={switchEn} variant='text'>
            <FormattedMessage id='en-US' />
          </Button>
        </ButtonGroup>

        <Button component={Link} to='auth' color='primary' variant='outlined'>
          <FormattedMessage id='login' />
        </Button>
      </Grid>
    </Grid>
  );
};
