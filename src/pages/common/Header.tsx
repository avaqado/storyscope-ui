import React from 'react';
import clsx from 'clsx';
import { makeStyles, Theme, Grid, Typography, Button } from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme: Theme) => ({
  mt8: {
    marginTop: theme.spacing(8)
  },
  textAlignCenter: {
    textAlign: 'center'
  },
  button: {
    background: 'linear-gradient(45deg, #2196F3 30%, #21CBF3 90%)',
    boxShadow: '0 3px 5px 2px rgba(33, 203, 243, .3)',
    border: 0,
    fontSize: 16,
    borderRadius: 3,
    color: 'white',
    height: 48,
    padding: '0 30px'
  }
}));

export const Header: React.FC = () => {
  const classes = useStyles();

  return (
    <Grid container direction='column' alignItems='center'>
      <Typography
        variant='h3'
        className={clsx(classes.mt8, classes.textAlignCenter)}
        gutterBottom
      >
        <FormattedMessage id='topic' />
      </Typography>

      <Typography variant='subtitle1' className={classes.textAlignCenter}>
        <FormattedMessage id='subtitle' />
      </Typography>

      <Button
        to='auth/register'
        component={Link}
        className={clsx(classes.mt8, classes.button)}
      >
        <FormattedMessage id='try' />
      </Button>
    </Grid>
  );
};
