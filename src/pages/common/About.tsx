import React from 'react';
import { Section } from './Section';
import { makeStyles } from '@material-ui/styles';
import { FormattedMessage } from 'react-intl';
import {
  Theme,
  createStyles,
  Stepper,
  Step,
  StepLabel,
  StepContent,
  Typography,
  Button,
  Grid
} from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '90%'
    },
    stepper: {
      background: '#000000'
    },
    button: {
      marginTop: theme.spacing(1),
      marginRight: theme.spacing(1)
    },
    actionsContainer: {
      marginBottom: theme.spacing(2)
    },
    resetContainer: {
      padding: theme.spacing(3)
    }
  })
);

export const About: React.FC = () => {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const steps = ['step1', 'step2', 'step3'];

  const handleNext = () => {
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };

  // const handleReset = () => {
  //   setActiveStep(0);
  // };

  return (
    <Section title='about' subtitle='aboutSubtitle'>
      <Grid container spacing={3} alignItems='center'>
        <Grid item md={6}>
          <img width='100%' alt='' src='images/how-work.png' />
        </Grid>
        <Grid item md={6}>
          <Stepper
            className={classes.stepper}
            activeStep={activeStep}
            orientation='vertical'
          >
            {steps.map((label, index) => (
              <Step key={label}>
                <StepLabel>
                  <FormattedMessage id={label} />
                </StepLabel>

                <StepContent>
                  <Typography>
                    <FormattedMessage id={`stepContent${index + 1}`} />
                  </Typography>

                  <div className={classes.actionsContainer}>
                    <div>
                      <Button
                        disabled={activeStep === 0}
                        onClick={handleBack}
                        className={classes.button}
                      >
                        <FormattedMessage id='back' />
                      </Button>
                      <Button
                        variant='outlined'
                        color='primary'
                        onClick={handleNext}
                        className={classes.button}
                      >
                        <FormattedMessage
                          id={
                            activeStep === steps.length - 1 ? 'finish' : 'next'
                          }
                        />
                      </Button>
                    </div>
                  </div>
                </StepContent>
              </Step>
            ))}
          </Stepper>
        </Grid>
      </Grid>
    </Section>
  );
};
