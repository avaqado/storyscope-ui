import React from 'react';
import clsx from 'clsx';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { makeStyles } from '@material-ui/styles';
import {
  Grid,
  Card,
  CardHeader,
  Typography,
  Divider,
  CardContent,
  List,
  ListItem,
  ListItemText,
  CardActions,
  Button,
  Theme,
  createStyles
} from '@material-ui/core';

interface PriceCardProps {
  title: string;
  price: number;
  items: string[];
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    textCenter: {
      textAlign: 'center'
    },
    mt6: {
      marginTop: theme.spacing(6)
    },
    card: {
      background: '#060B12'
    },
    button: {
      background: 'linear-gradient(45deg, #2196F3 30%, #21CBF3 90%)',
      boxShadow: '0 3px 5px 2px rgba(33, 203, 243, .3)',
      border: 0,
      fontSize: 16,
      borderRadius: 3,
      color: 'white',
      height: 40,
      padding: '0 30px'
    }
  })
);

export const PriceCard: React.FC<PriceCardProps> = ({
  title,
  price,
  items
}) => {
  const classes = useStyles();

  return (
    <Grid item xs={12} md={4}>
      <Card className={classes.card}>
        <CardHeader
          title={
            <Grid container justify='center'>
              <Typography variant='button'>
                <FormattedMessage id={title} />
              </Typography>
            </Grid>
          }
        />

        <Divider variant='middle' />

        <Typography
          className={clsx(classes.textCenter, classes.mt6)}
          variant='h3'
        >
          <FormattedNumber
            style={`currency`}
            currency='RUB'
            value={price}
            minimumFractionDigits={0}
            maximumFractionDigits={0}
          />
        </Typography>

        <CardContent>
          <List component='nav'>
            {items.map(item => (
              <ListItem key={item}>
                <ListItemText
                  className={classes.textCenter}
                  primary={<FormattedMessage id={item} />}
                />
              </ListItem>
            ))}
          </List>
        </CardContent>

        {/* <Divider variant='middle' /> */}

        <CardActions>
          <Grid container direction='column'>
            <Button className={classes.button}>
              <FormattedMessage id='try' />
            </Button>
          </Grid>
        </CardActions>
      </Card>
    </Grid>
  );
};
