import React from 'react';
import { Grid, Theme } from '@material-ui/core';
import { PriceCard } from './PriceCard';
import { makeStyles } from '@material-ui/styles';
import { Section } from './Section';

const useStyles = makeStyles((theme: Theme) => ({
  mt8: {
    marginTop: theme.spacing(8)
  },
  mt6: {
    marginTop: theme.spacing(6)
  },
  textAlignCenter: {
    textAlign: 'center'
  }
}));

export const Prices: React.FC = () => {
  const classes = useStyles();

  return (
    <>
      <Section title='cost' subtitle='costSubtitle'>
        <Grid className={classes.mt6} container direction='row' spacing={4}>
          <PriceCard
            title='3 days'
            price={0}
            items={['trialAccess', 'fullAccess']}
          />
          <PriceCard
            title='1 month'
            price={990}
            items={['payForAccount', 'fullAccess']}
          />
          <PriceCard
            title='1 week'
            price={300}
            items={['payForAccount', 'fullAccess']}
          />
        </Grid>
      </Section>
    </>
  );
};
