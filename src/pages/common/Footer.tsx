import React from 'react';
import { FormattedMessage } from 'react-intl';
import {
  Grid,
  makeStyles,
  Theme,
  Link as MuiLink,
  Typography,
  Box
} from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) => ({
  footer: {
    marginTop: theme.spacing(8),
    marginBottom: theme.spacing(4)
  },
  mr2: {
    marginRight: theme.spacing(1)
  },
  mt8: {
    marginTop: theme.spacing(8)
  },
  mt2: {
    marginTop: theme.spacing(2)
  },
  mb2: {
    marginBottom: theme.spacing(2)
  },
  pt2: {
    paddingTop: theme.spacing(2)
  }
}));

export const Footer: React.FC = () => {
  const classes = useStyles();

  return (
    <Grid container className={classes.footer} alignItems='flex-start'>
      <Box flexGrow={1}></Box>

      <Grid
        item
        sm={12}
        md={4}
        container
        direction='column'
        justify='flex-start'
        className={classes.mt2}
      >
        <div className={classes.mb2}>
          <img
            className={classes.mr2}
            src='images/mastercard-pay.svg'
            alt='mastercard'
          />
          <img
            className={classes.mr2}
            src='images/maestro-pay.svg'
            alt='maestro'
          />
          <img className={classes.mr2} src='images/visa-pay.svg' alt='visa' />
          <img className={classes.mr2} src='images/mir-pay.svg' alt='mir' />
        </div>

        <Typography variant='subtitle2' color='primary' gutterBottom>
          <FormattedMessage id='inn' />
        </Typography>
        <Typography variant='subtitle2' color='primary' gutterBottom>
          <FormattedMessage id='ogrn' />
        </Typography>
      </Grid>

      <Grid
        item
        sm={12}
        md={4}
        container
        direction='column'
        className={classes.pt2}
      >
        <MuiLink href='https://storyscope.ru/files/policy.pdf'>
          <Typography color='primary' gutterBottom>
            <FormattedMessage id='policy' />
          </Typography>
        </MuiLink>

        <MuiLink href='https://storyscope.ru/files/public_offer.pdf'>
          <Typography color='primary' gutterBottom>
            <FormattedMessage id='publicOffer' />
          </Typography>
        </MuiLink>
      </Grid>

      <Grid
        item
        sm={12}
        md={4}
        container
        direction='column'
        className={classes.pt2}
      >
        <MuiLink href='https://tele.gg/alisa_story'>
          <Typography color='primary' gutterBottom>
            <FormattedMessage id='support' />
          </Typography>
        </MuiLink>

        <MuiLink href='https://storyscope.ru/contacts'>
          <Typography color='primary' gutterBottom>
            <FormattedMessage id='contacts' />
          </Typography>
        </MuiLink>
      </Grid>
    </Grid>
  );
};
