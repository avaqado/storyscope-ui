import React from 'react';
import GroupIcon from '@material-ui/icons/Group';
import VisibilityIcon from '@material-ui/icons/Visibility';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { Avatar } from '../../components/Avatar';
import { makeStyles } from '@material-ui/styles';
import {
  CardHeader,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  createStyles,
  Theme
} from '@material-ui/core';
import { useTask } from 'hooks';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    list: {
      marginLeft: theme.spacing(1),
      paddingTop: theme.spacing(0)
    }
  })
);

export const Donor: React.FC = () => {
  const classes = useStyles();
  const [{ activeProfile }] = useTask();

  if (!activeProfile) return null;
  const { data, seenCount, parsedCount } = activeProfile;

  return (
    <>
      <CardHeader
        avatar={
          <Avatar username={data.username} src={data && data.profile_pic_url} />
        }
        title={<FormattedMessage id='donor' />}
        subheader={`@${data.username}`}
        titleTypographyProps={{
          variant: 'button',
          noWrap: true
        }}
      />

      <List className={classes.list} dense aria-label='data'>
        <ListItem>
          <ListItemIcon>
            <VisibilityIcon />
          </ListItemIcon>
          <ListItemText
            secondary={<FormattedMessage id='activeFollowers' />}
            primary={
              <FormattedNumber
                style={`percent`}
                value={seenCount / parsedCount}
              />
            }
          />
        </ListItem>

        <ListItem>
          <ListItemIcon>
            <GroupIcon />
          </ListItemIcon>
          <ListItemText
            secondary={<FormattedMessage id='parsed' />}
            primary={
              <FormattedMessage
                id='count'
                values={{
                  count: <FormattedNumber value={parsedCount} />,
                  entity: <FormattedMessage id='users' />
                }}
              />
            }
          />
        </ListItem>
      </List>
    </>
  );
};
