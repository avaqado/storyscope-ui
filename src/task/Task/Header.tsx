import React from 'react';
import { Avatar } from '../../components/Avatar';
import { CardHeader, IconButton } from '@material-ui/core';
import SyncIcon from '@material-ui/icons/Sync';
import { useTask, useTaskActions } from 'hooks';

export const Header: React.FC = () => {
  const [task] = useTask();
  const { info } = useTaskActions();
  const { account, name } = task;

  return (
    <CardHeader
      avatar={
        // <Badge
        //   color='secondary'
        //   overlap='circle'
        //   badgeContent={''}
        //   anchorOrigin={{
        //     vertical: 'bottom',
        //     horizontal: 'right'
        //   }}
        // >
        <Avatar
          username={account.username}
          src={account.data && account.data.profile_pic_url}
        />
        // </Badge>
      }
      title={name}
      subheader={`@${account.username}`}
      action={
        <IconButton onClick={info} aria-label='refresh'>
          <SyncIcon />
        </IconButton>
      }
      titleTypographyProps={{
        variant: 'button',
        noWrap: true
      }}
    />
  );
};
