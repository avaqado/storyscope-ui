import React from "react";
import Card from "@material-ui/core/Card";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import { Divider } from "@material-ui/core";
import { Stats } from "./Stats";
import { Donor } from "./Donor";
import { Header } from "./Header";
import { ActionMenu } from "./ActionMenu";
import { MenuItems } from "./MenuItems";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    card: {
      marginBottom: theme.spacing(3)
    }
  })
);

export const Task: React.FC = () => {
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <Header />
      <Stats />
      <Donor />
      <Divider variant="middle" light />
      <ActionMenu>
        <MenuItems />
      </ActionMenu>
    </Card>
  );
};
