import React from "react";
import GroupIcon from "@material-ui/icons/Group";
import VisibilityIcon from "@material-ui/icons/Visibility";
import QueryBuilderIcon from "@material-ui/icons/QueryBuilder";
import DetailsIcon from "@material-ui/icons/Details";
import { FormattedMessage, FormattedNumber } from "react-intl";
import { useTask } from "hooks";
import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  createStyles,
  makeStyles,
  Theme,
  Typography
} from "@material-ui/core";

type Color = "secondary" | "primary" | "error";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    list: {
      marginLeft: theme.spacing(1),
      paddingTop: theme.spacing(0)
    }
  })
);

export const Stats: React.FC = () => {
  const classes = useStyles();
  const [task] = useTask();
  const { seenCount, storiesCount, pendingCount, status } = task;

  return (
    <List className={classes.list} dense aria-label="data">
      <ListItem>
        <ListItemIcon>
          <DetailsIcon />
        </ListItemIcon>
        <ListItemText
          secondary={<FormattedMessage id="status" />}
          primary={
            <Typography variant="button" color={colorStatus(status)}>
              <FormattedMessage id={getStatus(status)} />
            </Typography>
          }
        />
      </ListItem>
      <ListItem>
        <ListItemIcon>
          <VisibilityIcon />
        </ListItemIcon>
        <ListItemText
          secondary={<FormattedMessage id="seen" />}
          primary={
            <FormattedMessage
              id="count"
              values={{
                count: <FormattedNumber value={storiesCount} />,
                entity: <FormattedMessage id="stories" />
              }}
            />
          }
        />
      </ListItem>

      <ListItem>
        <ListItemIcon>
          <QueryBuilderIcon />
        </ListItemIcon>
        <ListItemText
          secondary={<FormattedMessage id="pending" />}
          primary={
            <FormattedMessage
              id="count"
              values={{
                count: <FormattedNumber value={pendingCount} />,
                entity: <FormattedMessage id="stories" />
              }}
            />
          }
        />
      </ListItem>

      <ListItem>
        <ListItemIcon>
          <GroupIcon />
        </ListItemIcon>
        <ListItemText
          secondary={<FormattedMessage id="parsed" />}
          primary={
            <FormattedMessage
              id="count"
              values={{
                count: <FormattedNumber value={seenCount} />,
                entity: <FormattedMessage id="users" />
              }}
            />
          }
        />
      </ListItem>
    </List>
  );
};

function getStatus(status: number) {
  const schema = ["searching", "viewing", "stopping", "stopped", "finished"];
  return schema[status];
}

function colorStatus(status: number): Color {
  const schema: Color[] = [
    "secondary",
    "secondary",
    "error",
    "error",
    "primary"
  ];
  return schema[status];
}
