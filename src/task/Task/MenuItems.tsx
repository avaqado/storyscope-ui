import React, { useState } from 'react';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { makeStyles } from '@material-ui/styles';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import { useTaskActions, useTask } from 'hooks';
import {
  createStyles,
  Theme,
  Menu,
  MenuItem,
  IconButton,
  Divider,
  CircularProgress
} from '@material-ui/core';

type Anchor = null | HTMLElement;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    expand: {
      marginLeft: 'auto'
    },
    progress: {
      marginRight: theme.spacing(2),
      marginLeft: 'auto'
    }
  })
);

export const MenuItems: React.FC = () => {
  const [task] = useTask();
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState<Anchor>(null);

  const { _id: id, status } = task;
  const { restart, remove, resume, stop, menuLoading } = useTaskActions();

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => setAnchorEl(null);

  return (
    <>
      {menuLoading ? (
        <CircularProgress size={25} className={classes.progress} />
      ) : (
        <IconButton
          onClick={handleClick}
          className={classes.expand}
          aria-label='more'
        >
          <MoreVertIcon />
        </IconButton>
      )}

      <Menu
        open={Boolean(anchorEl)}
        keepMounted
        id='account-menu'
        anchorEl={anchorEl}
        onClick={handleMenuClose}
        onClose={handleMenuClose}
        onExit={handleMenuClose}
        getContentAnchorEl={null}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center'
        }}
        transformOrigin={{
          vertical: 'bottom',
          horizontal: 'right'
        }}
      >
        {(status === 3 || status === 4) && (
          <MenuItem dense onClick={restart}>
            <FormattedMessage id='restart' />
          </MenuItem>
        )}

        {status !== 3 && status !== 4 && (
          <MenuItem dense onClick={stop}>
            <FormattedMessage id='stop' />
          </MenuItem>
        )}

        {status === 3 && (
          <MenuItem dense onClick={resume}>
            <FormattedMessage id='resume' />
          </MenuItem>
        )}

        <MenuItem dense component={Link} to={`/tasks/${id}/stats`}>
          <FormattedMessage id='stats' />
        </MenuItem>

        <Divider variant='fullWidth' />

        <MenuItem dense onClick={remove}>
          <FormattedMessage id='remove' />
        </MenuItem>
      </Menu>
    </>
  );
};
