import React from 'react';
import { FormattedMessage } from 'react-intl';
import { CardActions, Button } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { useTask } from 'hooks';

export const ActionMenu: React.FC = ({ children }) => {
  const [task] = useTask();
  const id = task._id;

  return (
    <CardActions>
      <Button
        component={Link}
        to={`/tasks/${id}/edit`}
        size='small'
        color='primary'
      >
        <FormattedMessage id='edit' />
      </Button>

      {/* <Badge color='secondary' variant='dot'> */}
      <Button
        component={Link}
        to={`/tasks/${id}/records`}
        size='small'
        color='primary'
      >
        <FormattedMessage id='errors' />
      </Button>
      {/* </Badge> */}

      {children}
    </CardActions>
  );
};
