import React, { useState } from 'react';
import { AppContainer } from '../app/AppContainer';
import { Formik, FormikActions } from 'formik';
import { Redirect } from 'react-router';
import { handleFormError } from '../utils/handleResponse';
import { useIntl } from 'react-intl';
import { TaskValues } from '../types';
import { TaskApi } from '../api';
import { TaskUpdateSchema } from '../validations/TaskUpdate';
import { TaskForm } from './TaskForm';
import { TASKS } from '../api/routes';

export const TaskAdd = () => (
  <AppContainer title='taskAddTitle'>
    <AddForm />
  </AppContainer>
);

const initialTask: TaskValues = {
  name: '',
  account: '',
  list: '',
  parseSpeed: 'fast',
  seePerUser: 4,
  donorLimit: 0,
  seenUniq: false,
  restart: true,
  vote: true,
  justParse: false,
  onlyRussian: false,
  noArab: false,
  stopWords: false
};

const AddForm = () => {
  const intl = useIntl();
  const [needRedirect, setRedirect] = useState(false);

  const handleSubmit = (
    values: TaskValues,
    actions: FormikActions<TaskValues>
  ) => {
    TaskApi.create(values)
      .then(() => setRedirect(true))
      .catch(error => handleFormError(error, actions))
      .finally(() => actions.setSubmitting(false));
  };

  if (needRedirect) return <Redirect to={TASKS.ALL} />;

  return (
    <Formik
      initialValues={initialTask}
      onSubmit={handleSubmit}
      validationSchema={TaskUpdateSchema(intl)}
      render={props => <TaskForm {...props} />}
    />
  );
};
