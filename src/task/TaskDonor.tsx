import React from 'react';
import Card from '@material-ui/core/Card';
import { CardHeader, Box } from '@material-ui/core';
import { Avatar } from 'components/Avatar';
import { FormattedNumber, FormattedMessage } from 'react-intl';

interface TaskDonor {
  username: string;
  parsedCount: number;
  seenCount: number;
  data: {
    profile_pic_url: string;
  };
}

export const TaskDonor: React.FC<TaskDonor> = ({
  seenCount,
  parsedCount,
  username,
  data
}) => {
  return (
    <Box mt={2}>
      <Card>
        <CardHeader
          avatar={
            <Avatar username={username} src={data && data.profile_pic_url} />
          }
          subheader={
            <FormattedMessage
              id='activeFollowersPersent'
              values={{
                amount: (
                  <FormattedNumber
                    value={seenCount / parsedCount}
                    style={`percent`}
                  />
                )
              }}
            />
          }
          title={`@${username}`}
          titleTypographyProps={{
            variant: 'button',
            noWrap: true
          }}
        ></CardHeader>
      </Card>
    </Box>
  );
};
