import React, { useState } from 'react';
import { AppContainer } from '../app/AppContainer';
import { Formik, FormikActions } from 'formik';
import { TaskValues, IdMatchParams } from '../types';
import { Redirect, RouteComponentProps } from 'react-router';
import { handleFormError } from '../utils/handleResponse';
import { useIntl } from 'react-intl';
import { TaskApi } from '../api/taskApi';
import { TaskForm } from './TaskForm';
import { TaskUpdateSchema } from '../validations/TaskUpdate';
import { TASKS } from '../api/routes';
import { useApi } from '../hooks/useApi';
import { Loading } from '../components/Loading';

interface TaskEditProps extends RouteComponentProps<IdMatchParams> {}

export const taskEdit: React.FC<TaskEditProps> = ({ match }) => (
  <AppContainer title='taskEditTitle'>
    <EditForm id={match.params.id} />
  </AppContainer>
);

const EditForm: React.FC<IdMatchParams> = ({ id }) => {
  const intl = useIntl();
  const [needRedirect, setRedirect] = useState(false);
  const { data: task, isLoading, error } = useApi(`/tasks/${id}`);

  const handleSubmit = (
    values: TaskValues,
    actions: FormikActions<TaskValues>
  ) => {
    TaskApi.update(id, values)
      .then(() => setRedirect(true))
      .catch(error => handleFormError(error, actions))
      .finally(() => actions.setSubmitting(false));
  };

  if (isLoading) return <Loading />;
  if (error) return <>Error</>;
  if (needRedirect) return <Redirect to={TASKS.ALL} />;

  return (
    <Formik
      initialValues={task}
      onSubmit={handleSubmit}
      validationSchema={TaskUpdateSchema(intl)}
      render={props => <TaskForm {...props} type='edit' />}
    />
  );
};
