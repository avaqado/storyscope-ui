import React from 'react';
import { AppContainer } from 'app/AppContainer';
import { useApi } from 'hooks';
import { RouteComponentProps } from 'react-router';
import { IdMatchParams, RecordProps } from 'types';
import { Loading } from 'components/Loading';
import { FormattedMessage } from 'react-intl';
import { TaskRecord } from './TaskRecord';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  Paper,
  Box,
  TableBody
} from '@material-ui/core';

interface TaskRecordsProps extends RouteComponentProps<IdMatchParams> {}

export const TaskRecords: React.FC<TaskRecordsProps> = ({ match }) => {
  const { data, isLoading, error } = useApi(
    `/tasks/${match.params.id}/records`
  );

  if (isLoading) return <Loading />;
  if (error) return <>{error}</>;
  if (!data.length)
    return (
      <AppContainer
        title='errors'
        subtitle={<FormattedMessage id='noErrors' />}
      />
    );

  return (
    <AppContainer title='errors' subtitle={<Box my={2} />}>
      <Paper>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell></TableCell>
              <TableCell align='justify' padding='none'>
                <FormattedMessage id='error' />
              </TableCell>

              <TableCell>
                <FormattedMessage id='time' />
              </TableCell>
            </TableRow>
          </TableHead>

          <TableBody>
            {data.map((record: RecordProps) => (
              <TaskRecord key={record._id} {...record} />
            ))}
          </TableBody>
        </Table>
      </Paper>
    </AppContainer>
  );
};
