import React, { useState } from 'react';
import { RecordProps } from 'types';
import { FormattedMessage, FormattedTime } from 'react-intl';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';

import {
  Popover,
  Typography,
  TableRow,
  TableCell,
  IconButton,
  Paper,
  Box
} from '@material-ui/core';

export const TaskRecord: React.FC<RecordProps> = ({
  _id: id,
  params,
  createdAt,
  ...other
}) => {
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);

  const handleClick = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    setAnchorEl(event.currentTarget);
  };

  return (
    <>
      <TableRow key={id}>
        <TableCell padding='none'>
          <IconButton onClick={handleClick}>
            <InfoOutlinedIcon />
          </IconButton>
        </TableCell>
        <TableCell padding='none'>
          <FormattedMessage id={`error ${params}`} />
        </TableCell>
        <TableCell>
          <FormattedTime
            value={createdAt}
            year='numeric'
            month='numeric'
            day='numeric'
          />
        </TableCell>
      </TableRow>
      <RecordPopover anchor={anchorEl} setAnchor={setAnchorEl} {...other} />
    </>
  );
};

const RecordPopover: React.FC<any> = ({
  anchor,
  setAnchor,
  error,
  from,
  message
}) => {
  const handleClose = () => {
    setAnchor(null);
  };

  const open = Boolean(anchor);

  return (
    <Popover
      open={open}
      anchorEl={anchor}
      onClose={handleClose}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center'
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center'
      }}
    >
      <Paper>
        <Box p={2}>
          <Typography variant='subtitle1'>
            {from}: {error}
          </Typography>
          <Typography>{message}</Typography>
        </Box>
      </Paper>
    </Popover>
  );
};
