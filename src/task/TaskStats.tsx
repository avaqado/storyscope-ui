import React from 'react';
import { AppContainer } from 'app/AppContainer';
import { RouteComponentProps } from 'react-router';
import { IdMatchParams } from 'types';
import { useApi } from 'hooks';
import { Loading } from 'components/Loading';
import { FormattedMessage } from 'react-intl';
import { TaskDonor } from './TaskDonor';

interface TaskStatsProps extends RouteComponentProps<IdMatchParams> {}

export const TaskStats: React.FC<TaskStatsProps> = ({ match }) => {
  const { data, isLoading, error } = useApi(`tasks/${match.params.id}/stats`);

  if (isLoading) return <Loading />;
  if (error) return <>{error}</>;
  if (!data.length)
    return (
      <AppContainer
        title='taskStats'
        subtitle={<FormattedMessage id='noStats' />}
      />
    );

  return (
    <AppContainer title='taskStats'>
      {data.map((profile: any) => (
        <TaskDonor {...profile} />
      ))}
    </AppContainer>
  );
};
