import React from 'react';
import { TaskSkeleton } from './TaskSkeleton';
import { TaskProps } from '../types';
import { TaskProvider } from 'context/task';

interface TaskFeedProps {
  tasks: any;
  isLoading: boolean;
}

export const TaskFeed: React.FC<TaskFeedProps> = ({ tasks, isLoading }) => {
  return isLoading ? (
    <>
      <TaskSkeleton />
      <TaskSkeleton />
    </>
  ) : (
    tasks.map((task: TaskProps) => (
      <TaskProvider key={task._id} initialTask={task} />
    ))
  );
};
