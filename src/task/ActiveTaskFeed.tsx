import React from 'react';
import { useApi } from 'hooks';
import { TASKS } from 'api/routes';
import { AppContainer } from 'app/AppContainer';
import { TaskFeed } from './TaskFeed';
import { Typography } from '@material-ui/core';
import { FormattedMessage } from 'react-intl';

export const ActiveTaskFeed = () => {
  const { data, error, isLoading } = useApi(TASKS.ACTIVE);

  if (error) return <>{error}</>;
  return (
    <AppContainer
      title='taskList'
      subtitle={
        <Typography>
          <FormattedMessage
            id='taskCount'
            values={{ count: isLoading ? '...' : data.count }}
          />
        </Typography>
      }
    >
      <TaskFeed tasks={data && data.tasks} isLoading={isLoading} />
      {/* <Fab link={TASKS.ADD}>
        <AddIcon />
      </Fab> */}
    </AppContainer>
  );
};
