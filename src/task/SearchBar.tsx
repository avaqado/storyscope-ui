import React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import SortIcon from '@material-ui/icons/Sort';
import { FormattedMessage, useIntl } from 'react-intl';
import { Menu, MenuItem } from '@material-ui/core';

interface SearchBarProps {
  setParams: any;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: '2px 4px',
      display: 'flex',
      alignItems: 'center',
      marginBottom: theme.spacing(2)
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1
    },
    iconButton: {
      padding: 10
    },
    divider: {
      height: 28,
      margin: 4
    }
  })
);

export const SearchBar: React.FC<SearchBarProps> = ({ setParams }) => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const classes = useStyles();
  const intl = useIntl();

  const handleSortClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const setSorting = (sorting: string) =>
    setParams((prev: object) => ({ ...prev, sorting }));

  const handleSortClose = () => {
    setAnchorEl(null);
  };

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const search = event.target.value;
    setParams((prev: object) => ({ ...prev, search }));
  };

  return (
    <Paper className={classes.root}>
      <IconButton className={classes.iconButton} aria-label='menu'>
        <SearchIcon />
      </IconButton>
      <InputBase
        onChange={handleChange}
        className={classes.input}
        placeholder={intl.formatMessage({ id: 'search' })}
        inputProps={{ 'aria-label': 'search' }}
      />
      {/* <IconButton className={classes.iconButton} aria-label='search'>
        
      </IconButton> */}
      <Divider className={classes.divider} orientation='vertical' />

      <IconButton
        onClick={handleSortClick}
        color='primary'
        className={classes.iconButton}
        aria-label='directions'
      >
        <SortIcon />
      </IconButton>
      <Menu
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        onClick={handleSortClose}
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={handleSortClose}
        keepMounted
      >
        <MenuItem disabled dense>
          <FormattedMessage id='sorting' />
        </MenuItem>
        <MenuItem dense onClick={() => setSorting('status')}>
          <FormattedMessage id='byStatus' />
        </MenuItem>
        <MenuItem dense onClick={() => setSorting('statusReverse')}>
          <FormattedMessage id='byStatusReverse' />
        </MenuItem>
        <MenuItem dense onClick={() => setSorting('name')}>
          <FormattedMessage id='byName' />
        </MenuItem>
      </Menu>
    </Paper>
  );
};
