import React, { useState } from 'react';
import AddIcon from '@material-ui/icons/Add';
import { useGet } from 'hooks';
import { TASKS } from 'api/routes';
import { AppContainer } from 'app/AppContainer';
import { SubtitleLink } from 'components/SubtitleLink';
import { TaskFeed } from './TaskFeed';
import { Fab } from 'components/Fab';
import { SearchBar } from './SearchBar';

export const UserTaskFeed = () => {
  const [params, setParams] = useState({});
  const { data, error, isLoading } = useGet(TASKS.ALL, params);

  if (error) return <>{error}</>;
  return (
    <AppContainer
      title='taskList'
      subtitle={<SubtitleLink to={TASKS.ADD} title='taskAddTitle' />}
    >
      <SearchBar setParams={setParams} />
      <TaskFeed tasks={data} isLoading={isLoading} />
      <Fab link={TASKS.ADD}>
        <AddIcon />
      </Fab>
    </AppContainer>
  );
};
