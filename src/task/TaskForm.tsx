import React from 'react';
import { FormikProps, Form, Field } from 'formik';
import { FormattedMessage, useIntl } from 'react-intl';
import { TextField, Checkbox } from 'formik-material-ui';
import { Loading } from 'components/Loading';
import { useAuth } from 'context/auth';
import { TaskValues } from '../types';
import { ACCOUNTS } from 'api/routes';
import { useApi } from 'hooks';
import {
  Button,
  makeStyles,
  Theme,
  MenuItem,
  FormControlLabel,
  FormControl,
  Typography
} from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) => ({
  form: {
    width: '100%',
    marginTop: theme.spacing(2)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

interface TaskFormProps extends FormikProps<TaskValues> {
  type?: string;
}

const numbers = [1, 2, 3, 4, 5, 6, 7, 8];

export const TaskForm: React.FC<TaskFormProps> = ({
  type,
  values,
  isSubmitting
}) => {
  const speeds = ['fast', 'middle', 'slow'];
  const { user } = useAuth();

  const classes = useStyles();
  const intl = useIntl();
  const isAdmin = user.type === 'admin';

  const { data: accounts, isLoading, error } = useApi(ACCOUNTS.ALL);

  if (values.parseSpeed === 'flash') speeds.unshift('flash');
  if (isAdmin || values.parseSpeed === 'ultra') speeds.unshift('ultra');
  if (error) return <>{error}</>;
  if (isLoading) return <Loading />;

  return (
    <Form className={classes.form}>
      <Typography>
        <FormattedMessage id='illAlert' />
      </Typography>
      <Field
        label={intl.formatMessage({ id: 'name' })}
        id='name'
        name='name'
        variant='outlined'
        margin='normal'
        component={TextField}
        fullWidth
      />

      <Field
        label={intl.formatMessage({ id: 'account' })}
        component={TextField}
        id='account'
        name='account'
        variant='outlined'
        margin='normal'
        disabled={type === 'edit'}
        select
        required
        fullWidth
      >
        {accounts.map((account: any) => (
          <MenuItem key={account._id} value={account._id}>
            {`${account.username} ${account.comment}`}
          </MenuItem>
        ))}
      </Field>

      <Field
        label={intl.formatMessage({ id: 'profiles' })}
        component={TextField}
        id='list'
        name='list'
        variant='outlined'
        margin='normal'
        rows='5'
        multiline
        required
        fullWidth
      />

      {!values.vote && (
        <Field
          label={intl.formatMessage({ id: 'speed' })}
          component={TextField}
          id='parseSpeed'
          name='parseSpeed'
          variant='outlined'
          margin='normal'
          type='text'
          select
          required
          fullWidth
        >
          {speeds.map((speed: string) => (
            <MenuItem
              key={speed}
              value={speed}
              children={<FormattedMessage id={speed} />}
            />
          ))}
        </Field>
      )}

      {!values.vote && (
        <Field
          label={intl.formatMessage({ id: 'seePerUser' })}
          component={TextField}
          id='seePerUser'
          name='seePerUser'
          variant='outlined'
          margin='normal'
          select
          required
          fullWidth
        >
          {numbers.map((number: number) => (
            <MenuItem key={number} value={number} children={number} />
          ))}
        </Field>
      )}

      <Field
        label={intl.formatMessage({ id: 'donorLimit' })}
        component={TextField}
        id='donorLimit'
        name='donorLimit'
        variant='outlined'
        margin='normal'
        type='number'
        required
        fullWidth
      />

      <FormControl fullWidth>
        <FormControlLabel
          label={intl.formatMessage({ id: 'restartOnFinish' })}
          control={<Field id='restart' name='restart' component={Checkbox} />}
        />
      </FormControl>

      {/* <FormControl fullWidth>
        <FormControlLabel
          label={intl.formatMessage({ id: 'onlyRussian' })}
          control={
            <Field id='onlyRussian' name='onlyRussian' component={Checkbox} />
          }
        />
      </FormControl> */}

      {/* <FormControl fullWidth>
        <FormControlLabel
          label={intl.formatMessage({ id: 'noArab' })}
          control={<Field id='noArab' name='noArab' component={Checkbox} />}
        />
      </FormControl> */}

      {/* <FormControl fullWidth>
        <FormControlLabel
          label={intl.formatMessage({ id: 'stopWords' })}
          control={
            <Field id='stopWords' name='stopWords' component={Checkbox} />
          }
        />
      </FormControl> */}

      {/* <FormControl fullWidth>
        <FormControlLabel
          label={intl.formatMessage({ id: 'justParse' })}
          control={
            <Field id='justParse' name='justParse' component={Checkbox} />
          }
        />
      </FormControl> */}

      <FormControl fullWidth>
        <FormControlLabel
          disabled={type === 'edit'}
          label={intl.formatMessage({ id: 'vote' })}
          control={<Field id='vote' name='vote' component={Checkbox} />}
        />
      </FormControl>

      {values.vote && (
        <Field
          label={intl.formatMessage({ id: 'questionForStories' })}
          component={TextField}
          id='question'
          name='question'
          variant='outlined'
          margin='normal'
          rows='5'
          multiline
          fullWidth
        />
      )}

      {isAdmin && (
        <FormControl fullWidth>
          <FormControlLabel
            label={intl.formatMessage({ id: 'seenUniq' })}
            control={
              <Field id='seenUniq' name='seenUniq' component={Checkbox} />
            }
          />
        </FormControl>
      )}

      <Button
        type='submit'
        variant='contained'
        color='primary'
        size='large'
        disabled={isSubmitting}
        className={classes.submit}
      >
        <FormattedMessage id='save' />
      </Button>

      {/* <div>{JSON.stringify(values, null, 2)}</div> */}
    </Form>
  );
};
