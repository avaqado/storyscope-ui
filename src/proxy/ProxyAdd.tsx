import React, { useState } from 'react';
import { AppContainer } from '../app/AppContainer';
import { Formik, FormikActions } from 'formik';
import { ProxyValues } from '../types';
import { Redirect } from 'react-router';
import { handleFormError } from '../utils/handleResponse';
import { useIntl } from 'react-intl';
import { ProxyApi } from '../api/proxyApi';
import { PROXIES } from '../api/routes';
import { ProxyForm } from './ProxyForm';
import { ProxyUpdateSchema } from '../validations/ProxyUpdate';

export const ProxyAdd = () => (
  <AppContainer title='proxyAddTitle'>
    <AddForm />
  </AppContainer>
);

const AddForm = () => {
  const intl = useIntl();
  const [needRedirect, setRedirect] = useState(false);
  const [proxy] = useState<ProxyValues>({
    host: '',
    username: '',
    password: '',
    comment: '',
    user: ''
  });

  const handleSubmit = (
    values: ProxyValues,
    actions: FormikActions<ProxyValues>
  ) => {
    ProxyApi.create(values)
      .then(() => setRedirect(true))
      .catch(error => handleFormError(error, actions))
      .finally(() => actions.setSubmitting(false));
  };

  if (needRedirect) return <Redirect to={PROXIES.ALL} />;

  return (
    <Formik
      initialValues={proxy}
      onSubmit={handleSubmit}
      validationSchema={ProxyUpdateSchema(intl)}
      render={props => <ProxyForm {...props} />}
    />
  );
};
