import React, { useState } from 'react';
import DnsIcon from '@material-ui/icons/Dns';
import PersonIcon from '@material-ui/icons/Person';
import LockIcon from '@material-ui/icons/Lock';
import CommentIcon from '@material-ui/icons/Comment';
import { makeStyles } from '@material-ui/styles';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import { IProxy } from '../../types';
import { ProxyApi } from '../../api/proxyApi';
import { handleApiError } from '../../utils/handleResponse';
import { useSnackbar, useConfirmDialog } from '../../hooks';
import {
  createStyles,
  Theme,
  Card,
  Divider,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  CardActions,
  Button
} from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    card: {
      marginBottom: theme.spacing(3)
    }
  })
);

export const Proxy: React.FC<IProxy> = ({
  _id: id,
  host,
  username,
  password,
  comment
}) => {
  const [hide, setHide] = useState(false);
  const [showConfirm, clearConfirm] = useConfirmDialog();
  const addAlert = useSnackbar();
  const classes = useStyles();

  const handleConfirm = () => {
    ProxyApi.destroy(id)
      .then(() => setHide(true))
      .catch(e => {
        handleApiError(e);
        addAlert({ messageId: 'error' });
        setHide(false);
      });

    clearConfirm();
  };

  const handleRemoveClick = () => {
    showConfirm({
      handleConfirm,
      title: 'proxyRemoveDialogTitle'
    });
  };

  if (hide) return null;

  return (
    <Card className={classes.card}>
      <List aria-label='data'>
        <ListItem>
          <ListItemIcon>
            <DnsIcon />
          </ListItemIcon>
          <ListItemText
            primary={host}
            secondary={<FormattedMessage id='host' />}
          />
        </ListItem>

        <ListItem>
          <ListItemIcon>
            <PersonIcon />
          </ListItemIcon>
          <ListItemText
            primary={username}
            secondary={<FormattedMessage id='username' />}
          />
        </ListItem>

        <ListItem>
          <ListItemIcon>
            <LockIcon />
          </ListItemIcon>
          <ListItemText
            primary={password}
            secondary={<FormattedMessage id='password' />}
          />
        </ListItem>

        {comment && (
          <ListItem>
            <ListItemIcon>
              <CommentIcon />
            </ListItemIcon>
            <ListItemText
              primary={comment}
              secondary={<FormattedMessage id='comment' />}
            />
          </ListItem>
        )}
      </List>

      <Divider variant='middle' light />

      <CardActions>
        <Button component={Link} to={`proxies/${id}/edit`}>
          <FormattedMessage id='edit' />
        </Button>

        <Button onClick={handleRemoveClick}>
          <FormattedMessage id='remove' />
        </Button>
      </CardActions>
    </Card>
  );
};
