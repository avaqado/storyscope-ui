import React, { useState, useEffect } from 'react';
import AddIcon from '@material-ui/icons/Add';
import { AppContainer } from '../app/AppContainer';
import { Redirect } from 'react-router-dom';
import { PROXIES } from '../api/routes';
import { ProxySkeleton } from './ProxySkeleton';
import { ProxyApi } from '../api/proxyApi';
import { IProxy } from '../types';
import { Proxy } from './ProxyModel';
import { Fab } from '../components/Fab';
import { SubtitleLink } from '../components/SubtitleLink';

export const ProxyFeed = () => {
  const [data, setData] = useState([]);
  const [isLoading, setLoading] = useState(true);
  const [errorMessage, setError] = useState(null);

  useEffect(() => {
    setLoading(true);
    setError(null);

    ProxyApi.all()
      .then(res => {
        setData(res.data);
        setLoading(false);
      })
      .catch(e => {
        setError(e.response.status);
      });
  }, []);

  if (errorMessage) return <>{errorMessage}</>;
  if (!isLoading && !data.length) return <Redirect to={PROXIES.ADD} />;

  return (
    <AppContainer
      title='proxyList'
      subtitle={<SubtitleLink to={PROXIES.ADD} title='proxyAddTitle' />}
    >
      {isLoading ? (
        <>
          <ProxySkeleton />
          <ProxySkeleton />
          <ProxySkeleton />
        </>
      ) : (
        data.map((proxy: IProxy) => <Proxy key={proxy._id} {...proxy} />)
      )}

      <Fab link={PROXIES.ADD}>
        <AddIcon />
      </Fab>
    </AppContainer>
  );
};
