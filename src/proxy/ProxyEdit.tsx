import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { AppContainer } from '../app/AppContainer';
import { Formik, FormikActions } from 'formik';
import { ProxyValues, IdMatchParams } from '../types';
import { Redirect, RouteComponentProps } from 'react-router';
import { handleFormError } from '../utils/handleResponse';
import { useIntl } from 'react-intl';
import { ProxyApi } from '../api/proxyApi';
import { PROXIES } from '../api/routes';
import { ProxyForm } from './ProxyForm';
import { ProxyUpdateSchema } from '../validations/ProxyUpdate';
import { Typography } from '@material-ui/core';

interface ProxyEditProps extends RouteComponentProps<IdMatchParams> {}

export const ProxyEdit: React.FC<ProxyEditProps> = ({ match }) => (
  <AppContainer title='proxyAddTitle'>
    <EditForm id={match.params.id} />
  </AppContainer>
);

const EditForm: React.FC<IdMatchParams> = ({ id }) => {
  const intl = useIntl();
  const [isLoading, setLoading] = useState(false);
  const [errorMessage, setError] = useState(null);
  const [needRedirect, setRedirect] = useState(false);
  const [proxy, setProxy] = useState<ProxyValues>({
    host: '',
    username: '',
    password: '',
    comment: '',
    user: ''
  });

  const handleSubmit = (
    values: ProxyValues,
    actions: FormikActions<ProxyValues>
  ) => {
    ProxyApi.update(id, values)
      .then(() => setRedirect(true))
      .catch(error => handleFormError(error, actions))
      .finally(() => actions.setSubmitting(false));
  };

  useEffect(() => {
    let unmounted = false;
    let source = axios.CancelToken.source();

    setLoading(true);
    setError(null);

    ProxyApi.getOne(id)
      .then(res => {
        if (unmounted) return;

        const { host, username, password, comment, user } = res.data;
        setProxy({ host, username, password, comment, user });
        setLoading(false);
      })
      .catch(e => setError(e.response.status));

    return () => {
      unmounted = true;
      source.cancel('Cancelling in cleanup');
    };
  }, [id]);

  if (errorMessage) return <>{errorMessage}</>;
  if (isLoading) return <Typography>Loading...</Typography>;
  if (needRedirect) return <Redirect to={PROXIES.ALL} />;

  return (
    <Formik
      initialValues={proxy}
      onSubmit={handleSubmit}
      validationSchema={ProxyUpdateSchema(intl)}
      render={props => <ProxyForm {...props} />}
    />
  );
};
