import React from 'react';
import { FormikProps, Form, Field } from 'formik';
import { FormattedMessage, useIntl } from 'react-intl';
import { ProxyValues } from '../types';
import { Button, makeStyles, Theme } from '@material-ui/core';
import { TextField } from 'formik-material-ui';

const useStyles = makeStyles((theme: Theme) => ({
  form: {
    width: '100%',
    marginTop: theme.spacing(2)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

export const ProxyForm = (props: FormikProps<ProxyValues>) => {
  const { isSubmitting } = props;
  const classes = useStyles();
  const intl = useIntl();

  return (
    <Form className={classes.form}>
      <Field
        label={intl.formatMessage({ id: 'host' })}
        component={TextField}
        id='host'
        name='host'
        variant='outlined'
        margin='normal'
        required
        fullWidth
      />

      <Field
        label={intl.formatMessage({ id: 'username' })}
        component={TextField}
        id='username'
        name='username'
        variant='outlined'
        margin='normal'
        type='text'
        autoComplete='nope'
        required
        fullWidth
      />

      <Field
        label={intl.formatMessage({ id: 'password' })}
        component={TextField}
        id='password'
        name='password'
        variant='outlined'
        margin='normal'
        type='text'
        autoComplete='new-password'
        required
        fullWidth
      />

      <Field
        label={intl.formatMessage({ id: 'comment' })}
        component={TextField}
        id='comment'
        name='comment'
        variant='outlined'
        margin='normal'
        type='text'
        fullWidth
      />

      <Button
        type='submit'
        variant='contained'
        color='primary'
        size='large'
        disabled={isSubmitting}
        className={classes.submit}
      >
        <FormattedMessage id='save' />
      </Button>
    </Form>
  );
};
