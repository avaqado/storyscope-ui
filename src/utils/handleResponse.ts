import { FormikActions } from 'formik';

// export const handleStatus = (status: number) => {
//   if ([401, 403].includes(status)) {
//     // authenticationService.logout();
//     location.reload(true);
//   }

//   // const error = (data && data.message) || response.statusText;
//   // return Promise.reject(error);
// };

export const handleFormError = (error: any, actions: FormikActions<any>) => {
  console.log(error.response.data);

  switch (error.response.status) {
    case 422:
      actions.setErrors(error.response.data.errors);
      break;
    case 500:
      console.log('Form error', error.response.data);
      break;
    default:
      actions.setStatus({ msg: error.response.data.errors });
  }
};

export const handleLoginError = (
  error: any,
  actions: FormikActions<any>,
  message: string
) => {
  if (error.response)
    switch (error.response.status) {
      case 401:
        actions.setFieldError('password', message);
        break;
    }
  else actions.setStatus('unkownError');
};

export const handleRegisterError = (
  error: any,
  actions: FormikActions<any>
) => {
  if (error.response) actions.setStatus(error.response.data);
  else actions.setStatus('Unkown Error');
};

export const handleApiError = (error: any) => {
  switch (error.response.status) {
    case 400:
      console.log('Request error', error.response.data);
      break;
    case 500:
      console.log('Server error', error.response.data);
      break;
    default:
      console.log('Unknown error', error.response.data);
  }
};
